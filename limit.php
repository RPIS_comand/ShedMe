<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Ограничения:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
         // подключаем общий фрагмент
// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=limit.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 4; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
}
// обновить
if(isset($_POST['limID']))
{
    $id = trim(stripslashes($_POST['limID']));
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    // перебрать все поля
    $hidden_fields = array('ID','curID','limID');
    
    $ins_data = "";
    foreach($_POST as $parname => $value) {
        if( ! in_array($parname, $hidden_fields))
        $ins_data .= (empty($ins_data)? "" : ",") . $parname ."=". $value;
    }

    /// print_r($ins_data);
    
    PDOexec("UPDATE `limits` SET $ins_data WHERE ID=$id;");
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");

    $Message = "Сохранено.";
}        
      
  ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <small><a href="index.php">Домой</a></small></h3>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>

<!-- Навигация -->
<?php
    insert_navigation_pills("limit");
?>
      
<pre><?php

    /// print_r("POST: ");    print_r($_POST);
    
    // лимиты
    $records = PDOfetchAll("SELECT * FROM `limits` WHERE curID=$ctrl->curID");      
    
    if(empty($records)) {
        // добавить лимиты к плану
        $ok = PDOexec("INSERT INTO `limits` (curID) VALUES($ctrl->curID);");
        $records = PDOfetchAll("SELECT * FROM `limits` WHERE curID=$ctrl->curID");      
    }
              
      // лимиты - запись со значениями ограничений
      $rec = $records[0];

      $hidden_fields = array('ID','curID');
      $disabled_fields = array('rooms');
      
      $decorated_fields['days'] =         array('name' => 'Дней в расписании',
                                        'default' => 6);
      $decorated_fields['lessons'] =      array('name' => 'Занятий в день',
                                        'default' => 6);
      $decorated_fields['lessons_prof'] = array('name' => 'Максимальная нагрузка на преподавателя в день',
                                        'default' => '4 занятия');
      $decorated_fields['lessons_stud'] = array('name' => 'Максимальная нагрузка на группу студентов в день',
                                        'default' => '4 занятия');
      $decorated_fields['rooms'] =        array('name' => 'Количество доступных <a href="room.php">аудиторий</a>',
                                        'default' => false);
              
    /// print_r($rec);
    
    if(isset($Message))
    {
        echo $Message;
    }
              /*
              Лекций:
              Практик:
              Лабораторных работ: // */
?></pre>
      
<h3> 
      
<ol class="breadcrumb">
  <li><a href="limit.php">Ограничения</a></li>
  <?php if(false) { ?>
    <li><a href="limit.php?expert=1"
        >Расширенные настройки</a></li>
    <?php } ?>
</ol>      
      
</h3>
      
<div class="container-fluid content">

        <u><h4>Настройки и ограничения для текущего учебного плана</h4></u>
        <form method="POST" action="">
            <div class="form-group text-center">
            <input type="hidden" name="limID" value="<?php echo $rec['ID'] ?>">

<?php foreach($rec as $key => $value) { 
        if(in_array($key, $hidden_fields)) continue; ?>
        <p class="text-left indent"><?php echo ($decorated_fields[$key]['name'] ?: $key) . ($decorated_fields[$key]['default'] ?  " (по умолчанию ".$decorated_fields[$key]['default'].")": '').':' ?></p>
        <input type="number" name="<?php
            echo $key 
         ?>" class="form-control indent" min="1" value="<?php
            echo $value 
         ?>" required <?php
            if(in_array($key, $disabled_fields)) echo "disabled";
         ?>>
<?php } ?>
            </div>
            <div class="logModal-footer">
                <input type="submit" class="btn btn-primary" value="Сохранить">
            </div>
        </form>
              
      
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>