<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Преподаватели:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
         // подключаем общий фрагмент
         include_once "prof_header.php";

        // SAVE
        $ctrl->saveToSESSION();
        // SAVE
  ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <small><a href="index.php">Домой</a></small></h3>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>

<!-- Навигация --><!-- 
<ul class="nav nav-pills">
  <li><a href="index.php">Домой</a></li>
  <li class="active"><a href="">Преподаватели</a></li>
  <li><a href="group.php">Группы и занятия</a></li>
  <li><a href="summary.php">Сводка</a></li>
  <li><a href="limit.php">Ограничения</a></li>
  <li><a href="calc.php">Расчёт</a></li>
  <li><a href="view.php">Просмотр</a></li>
</ul>      -->
<?php
    insert_navigation_pills("prof");
?>
      
<pre><?php
     //// include_once "common/modals.html"; // подключаем общий фрагмент
      include_once "common.php"; // функции

    $records = PDOfetchAll("SELECT * FROM `professor` WHERE curID=$ctrl->curID ORDER BY name ASC");      
    
    if($profID) {
        $profname = $records[array_search($profID, array_column($records, 'ID'))]['name']; 
        $nestname = false;

        // "вложенные" записи - предметы, принадлежащие Преподавателям
        $nestedrecs = PDOfetchAll("SELECT * FROM `subject` WHERE profID=$profID ORDER BY course,name");  
        if(count($nestedrecs) > 0) {
            $nestrec = $nestedrecs[array_search($subjID, array_column($nestedrecs, 'ID'))];
            $nestname = $nestrec['name']; 
            $nestcourse = $nestrec['course']; 
            // NULL -> 0
            $nestcourse = is_null($nestcourse)? 0 : $nestcourse;

            unset($nestrec);
        }
    }
              
    if(isset($Message))
    {
        echo $Message;
    }
   //echo "\$profID=".$profID;
    //// $key = array_search(40489, array_column($userdb, 'uid'));
    ///  print_r(array_column($records, 'name'));
              
    ///  print_r(array_search($profID, array_column($records, 'ID')));
    // print_r ($nestedrecs);
?></pre>
      
<h3> 
      
<ol class="breadcrumb">
  <li><a href="prof.php">Преподаватели</a></li>
  <?php if($profID) { ?>
    <li <?php if( ! $subjID) echo 'class="active"' ?>>
        <a href="prof.php?p=<?php echo $profID ?>"
        ><?php echo $profname ?></a></li>
    <?php } ?>
  <?php if($subjID) { ?>
    <li <?php echo 'class="active"' ?>>
        <a href="prof.php?p=<?php echo $profID ?>&s=<?php echo $subjID ?>"
        ><?php echo $nestname ?></a></li>
    <?php } ?>
</ol>      
      
</h3>
      
      <!-- Расположить таблицы рядом по горизонтали text-center -->
<div class="container-fluid content">
  <div class="row">
  <div class="col-md-3  col-sm-3">
      <table class="table table-condensed table-hover" style="width:20%">
      <thead>
        <tr>
          <th>№</th>
          <th>Преподаватель</th>
          <td><small><i>Удалить</i></small></td>
        </tr>
      </thead>
      <tbody>
          <!-- При клике по ссылке выбранный Преподаватель становится текущим -->
        <?php foreach ($records as $i => $rec) { ?>
        <tr <?php if(($profID) and $profID == $rec['ID']) echo 'class="success"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><a href="?p=<?php echo $rec['ID'] ?>"><?php echo $rec['name'] ?></a></td>
          <td><button type="button" class="close" aria-hidden="true" onClick="var d=confirm('Удалить преподавателя? \nИмя преподавателя: <?php echo $rec['name'] ?>');if(d){window.location.href = '?del=<?php echo $rec['ID'] ?>';}"><img src='img/del.png' title="Удалить" alt="Удалить"></button></td>
        </tr>
        <?php } ?>
          
        <tr>
          <th><img src="img/add.png" /></th>
            <td colspan="2"><i><button type="button" class="btn btn-default" onClick="var n=prompt('Наименование нового преподавателя (до 25 символов):','Иванов');if(n){window.location.href = '?new='+n.slice(0,29);}">Создать преподавателя</button></i></td>
          <!-- <td></td> -->
        </tr>
          
      </tbody>
    </table>
 </div>
      
    <?php if($profID) { // преподователь выбран ?>
<div class="col-md-4 col-sm-4">      
      <table class="table table-condensed table-hover" style="width:20%">
      <thead>
        <tr>
          <th>№</th>
          <th>Предмет</th>
          <th>Курс</th>
          <td ><small><i>Удалить</i></small></td>
        </tr>
      </thead>
      <tbody>
          <!-- При клике по ссылке выбранный Предмет становится текущим -->
        <?php foreach ($nestedrecs as $i => $rec) { ?>
        <tr <?php if(($subjID) and $subjID == $rec['ID']) echo 'class="success"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><a href="?p=<?php echo $profID ?>&s=<?php echo $rec['ID'] ?>"><?php  echo $rec['name']; ?></a></td>
          <td><?php echo ($rec['course']==0)? "-" : "".$rec['course']."";  ?></td>
          <td><button type="button" class="close" aria-hidden="true" onClick="var d=confirm('Удалить предмет? \nИмя предмета: <?php echo $rec['name'] ?> \nИмя преподавателя: <?php echo $profname ?>');if(d){window.location.href = 'prof.php?p=<?php echo $profID ?>&delnest=<?php echo $rec['ID'] ?>';}"><img src='img/del.png' title="Удалить"></button></td>
        </tr>
        <?php } ?>
          
        <tr>
          <th><img src="img/add.png" /></th>
          <td colspan="2"><i><button type="button" class="btn btn-default" onClick="var n=prompt('Название предмета (сокращайте названия, 20 символов максимум):','Предмет');if(n){window.location.href = 'prof.php?p=<?php echo $profID ?>&newnest='+n.slice(0,29);}">Создать предмет</button></i></td>
          <td></td>
        </tr>
          
      </tbody>
    </table>
 </div>
    <?php } ?>

    <?php if($subjID) { // предмет выбран 
      
      ?>
    <div class="col-md-3 col-sm-3">
        <br>
        Дисциплина: <b><?php echo $nestname ?></b><br>
        <br>
        № курса дисциплины (<?php echo ($nestcourse==0)? 'не выбран' : $nestcourse; ?>):<br>
        <form>
        <select name='course' class="form-control" onChange="/*alert(course.selectedIndex);*/if(course.selectedIndex!=<?php echo $nestcourse ?>){window.location.href = 'prof.php?p=<?php echo $profID ?>&s=<?php echo $subjID ?>&course='+course.selectedIndex;};">
          <option value='0' <?php if($nestcourse==0) echo 'selected'; ?> >Не выбрано</option>
          <option value='1' <?php if($nestcourse==1) echo 'selected'; ?> >1</option>
          <option value='2' <?php if($nestcourse==2) echo 'selected'; ?> >2</option>
          <option value='3' <?php if($nestcourse==3) echo 'selected'; ?> >3</option>
          <option value='4' <?php if($nestcourse==4) echo 'selected'; ?> >4</option>
          <option value='5' <?php if($nestcourse==5) echo 'selected'; ?> >5</option>
          <option value='6' <?php if($nestcourse==6) echo 'selected'; ?> >6</option>
        </select>
        </form>
        
     <?php 
    $recs = PDOfetchAll("SELECT `group`.ID,`group`.name,`group`.course FROM `lessonplan`,`group` WHERE subjID=$subjID AND `lessonplan`.groupID=`group`.ID ORDER BY `group`.name ASC");      
    
        if(count($recs)>0) echo "<h5>Группы, слушающие дисциплину <small>(".count($recs).")</small>:</h5>";
              foreach($recs as $rec) {
                  echo "<li><a href='group.php?g=".$rec['ID']."&s=$subjID'>".
                      (!($rec['course']) ? "" : "[".$rec['course']."] ")
                      .$rec['name'].
                      " &#9998;</a></li>";
              } 
        unset($recs);
        ?>
        
        
    </div>
    <?php } ?>
      
 </div>
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>