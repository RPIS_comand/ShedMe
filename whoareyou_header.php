<?php 

/// session_destroy();session_start();
//unset($_SESSION['Controller']); // for debugging

//* get Controller
//$ctrl = Controller::loadFromSESSION();

//* юзер не представился
if(is_null($ctrl->usertype)) //// (!isset($_SESSION['Controller']))
{
    // Новый Controller
    $ctrl = new Controller();
    $ctrl->usertitle = 'Неизвестный пользователь';

    //print_r("new \$ctrl created\n");

    // see at the end of page
    //// $ctrl->saveToSESSION();

    //// header('Refresh: 0; url=whoareyou.php');
} // */
elseif(!isset($_GET['i']))
{
    redirectUserHome();
}


///    print_r($_SESSION);
    // $ctrl->message = 'hello';

    // if(! is_null($ctrl->usertype) )
if(isset($_GET['i']))
{
    $ok = true;
    
    $i = $_GET['i'];
    
    // Установка текущего юзера
    switch($i)
    {
        case 'admin':
            $ctrl->usertype = 'admin';
            $ctrl->usertitle = "Администратор";
            //header('Refresh: 0; url=index.php');
            break;
            
        case 'professor':
            if(isset($_GET['cur']))
            {
                $cur = $_GET['cur'];
                $ctrl->curID = $cur;
                $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$cur")['name'];
            } else {
                $ok = false;
            }
            if(!isset($_GET['id'])) {
                $ok = false;
                break;
            }
            $id = $_GET['id'];
            
            $ctrl->usertype = 'professor';
            $ctrl->profID = $id;
            $ctrl->usertitle = "Преподаватель ".PDOfetch("SELECT name FROM `professor` WHERE ID=$id")['name'];

            
            //header('Refresh: 0; url=view.php');
            break;
            
        case 'student':
            if(isset($_GET['cur']))
            {
                $cur = $_GET['cur'];
                $ctrl->curID = $cur;
                $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$cur")['name'];
            } else {
                $ok = false;
            }
            if(!isset($_GET['group'])) {
                $ok = false;
                break;
            }
            $group = $_GET['group'];
            
            $ctrl->usertype = 'student';
            $ctrl->groupID = $group;
            $ctrl->usertitle = "Студент группы &laquo;" . PDOfetch("SELECT name FROM `group` WHERE ID=$group")['name'] . "&raquo;";
            
            //header('Refresh: 0; url=view.php');
            break;
            
        case 'room':
            if(isset($_GET['cur']))
            {
                $cur = $_GET['cur'];
                $ctrl->curID = $cur;
                $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$cur")['name'];
            } else {
                $ok = false;
            }
            if(!isset($_GET['room'])) {
                $ok = false;
                break;
            }
            $room = $_GET['room'];
            
            $ctrl->usertype = 'room';
            $ctrl->roomID = $room;
            $ctrl->usertitle = "Вы в аудитории &laquo;" . PDOfetch("SELECT name FROM `room` WHERE ID=$room")['name'] . "&raquo;";
            
            //header('Refresh: 0; url=view.php');
            break;
            
        default:
            $ok = false;
    }
    
    if($ok) {
        // success
        $ctrl->saveToSESSION();
        redirectUserHome();
        exit("Going to Default page...");
    } else {
        $ctrl->message = 'Ошибка входа. Повторите выбор.';
    }
}

    
function redirectUserHome()
{
    global $ctrl;
    if($ctrl->usertype == "admin")
        header('Refresh: 0; url=index.php');
    else
        header('Refresh: 0; url=viewmy.php');
}

// */

?>