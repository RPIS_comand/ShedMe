<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Моё:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
      // Режим
      if($ctrl->usertype == "admin")
      {
            header('Refresh: 0; url=view.php'); // GO to view
            exit("Login as professor of student! Going to common view...");
      }
      
      $isProfessor = $ctrl->usertype == "professor";
      $isStudent = $ctrl->usertype == "student";
      $isRoom = $ctrl->usertype == "room";
      
         // подключаем общий фрагмент
// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=viewmy.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 2; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
} // */      
      
      
  ?>
  </head>
  <body>
<?php
     include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <!-- <small><a href="index.php">Домой</a></small>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5> --> </h3>


<!-- Навигация -->
<ul class="nav nav-pills">
    <li><a href="view.php">Общее расписание</a></li>
    <li class="active"><a href="viewmy.php">Моё расписание</a></li>
</ul>
<!-- / Навигация -->


<pre><?php
    $typeHr = array("lec" => "лекция","prac" => "практ.","lab" => "л/р");
    
// === === === === === === === === === === === === === === === === === === ===
function content_for($pos) {
    global $isProfessor,$isStudent,$isRoom, $records, $ctrl, $typeHr; //, $groups, $professors;
    
    // найти текущий lessonpos
    $lpos = null;
    foreach($records as $r) {
        if($pos == $r["pos"])
        {
            $lpos = $r;
            break;
        }
    }
    if(is_null($lpos)) 
        return "";

    
    if($isProfessor)
    {
        // groups on current pos
        $groups = PDOfetchAll("SELECT name FROM `group` WHERE curID=$ctrl->curID AND is_group_at_lesson(ID,".$lpos["lessonID"].");");
        
        if(empty($groups))
        {
            return "";
        }
        
        $str_group_list = "";
        foreach($groups as $g_item) {
            $str_group_list .= (empty($str_group_list)? "":", ") ."<b>". $g_item["name"] ."</b>";
        }
            
//                     PDOexec("SELECT
//                     lesson_get_subject(".$r["lessonID"]."), lesson_get_prof(".$r["lessonID"].")
//                     INTO @tmp_subjID,@tmp_profID");
        
       $subjName = PDOfetch("SELECT name FROM subject WHERE profID=$ctrl->profID AND ID=lesson_get_subject(".$lpos["lessonID"].");")["name"];
//        $profName = PDOfetch("SELECT name FROM professor WHERE ID=@tmp_profID;")["name"];
       $lessonType = PDOfetch("SELECT type FROM lesson WHERE ID=".$lpos["lessonID"].";")["type"];
       $lessonType = $typeHr[$lessonType];

       $roomName = PDOfetch("SELECT name FROM room WHERE ID=".$lpos["roomID"].";")["name"];
        
//             join( ", ", $workers )
        return  "$str_group_list <small><i>$lessonType</i><table width=100%><tr><td><i>$roomName</i></td><td align='right'>$subjName</td></tr></table></small>";
//                    $cell_content = "<b>$subjName</b> <small><i>$lessonType</i><table width=100%><tr><td><i>$roomName</i></td><td align='right'>$profName</td></tr></table></small>";
    }
    elseif($isStudent)
    {
//         $isAt = PDOfetch("SELECT is_group_at_lesson($ctrl->groupID,".$lpos["lessonID"].") as isAt;")["isAt"];
//         if( '0' == $isAt)
//         {
//             return "";
//         }
        
       $subjName = PDOfetch("SELECT name FROM subject WHERE ID=lesson_get_subject(".$lpos["lessonID"].");")["name"];
       $profName = PDOfetch("SELECT name FROM professor WHERE ID=lesson_get_prof(".$lpos["lessonID"].");")["name"];
       $lessonType = PDOfetch("SELECT type FROM lesson WHERE ID=".$lpos["lessonID"].";")["type"];
       $lessonType = $typeHr[$lessonType];

       $roomName = PDOfetch("SELECT name FROM room WHERE ID=".$lpos["roomID"].";")["name"];
        
//             join( ", ", $workers )
        return  "<b>$subjName</b> <small><i>$lessonType</i><table width=100%><tr><td><i>$roomName</i></td><td align='right'>$profName</td></tr></table></small>";
    }
    elseif($isRoom)
    {
        // groups on current pos
        $groups = PDOfetchAll("SELECT name FROM `group` WHERE curID=$ctrl->curID AND is_group_at_lesson(ID,".$lpos["lessonID"].");");
        
        if(empty($groups))
        {
            return "";
        }
        
        $str_group_list = "";
        foreach($groups as $g_item) {
            $str_group_list .= (empty($str_group_list)? "":", ") ."<b>". $g_item["name"] ."</b>";
        }
            
        
       $subjName = PDOfetch("SELECT name FROM subject WHERE ID=lesson_get_subject(".$lpos["lessonID"].");")["name"];
       $profName   = PDOfetch("SELECT name FROM professor WHERE ID=lesson_get_prof(".$lpos["lessonID"].");")["name"];
       $lessonType = PDOfetch("SELECT type FROM lesson WHERE ID=".$lpos["lessonID"].";")["type"];
       $lessonType = $typeHr[$lessonType];

       // $roomName = PDOfetch("SELECT name FROM room WHERE ID=".$lpos["roomID"].";")["name"];
        
        return  "$subjName <small><i>$lessonType</i><table width=100%><tr><td><i>$str_group_list</i></td><td align='right'>$profName</td></tr></table></small>";
    }
    
//    return  "<b>". "main_content" ."</b> <small><i>\$lessonType</i></small><div align='right'><small>\$subjName</small></div>";
    
} // === === === === === === === === === === === === === === === === === === ===
              // END OF function content_for()

   
              
    /// print_r("POST: ");    print_r($_POST);

    $is_cur_altered = PDOfetch("SELECT altered FROM `curriculum` WHERE curID=$ctrl->curID")["altered"] != 0;
//     print_r( $is_cur_altered );
//     print_r( "<<<" );
    if($is_cur_altered) 
    { ?><font color=red><H3>Внимание! После расчёта расписания учебный план был изменён.<br/><small>Возможно некорректное отображение данных. Обратитесь к Администратору Вашего учебного плана.</small></H3></font><?php 
    }
    
    // кол-во уроков
    $daylessons = PDOfetch("SELECT lessons FROM `limits` WHERE curID=$ctrl->curID")["lessons"];      
          $days = PDOfetch("SELECT    days FROM `limits` WHERE curID=$ctrl->curID")["days"];      
              
    // первое попавшееся расписание без учёта рейтинга (см. view.php, calc.php)
    $shedID = PDOfetch("SELECT ID FROM `schedule` WHERE curID=$ctrl->curID")["ID"];

//   print_r($shedID);
              
        
    $q = "";
    if($isProfessor)
    {
        $q = "SELECT ID,lessonID,pos,roomID
        FROM `lessonpos` WHERE shedID=$shedID AND pos>=0 AND $ctrl->profID=lesson_get_prof(lessonID)";
    }
    elseif($isStudent)
    {
        $q = "SELECT ID,lessonID,pos,roomID
        FROM `lessonpos` WHERE shedID=$shedID AND pos>=0 AND is_group_at_lesson($ctrl->groupID,lessonID)";
    }
    elseif($isRoom)
    {
        $q = "SELECT ID,lessonID,pos,roomID
        FROM `lessonpos` WHERE shedID=$shedID AND pos>=0 AND roomID = $ctrl->roomID;";
    }
              
    $records = PDOfetchAll($q);

//   print_r($records);
  echo " Занятий моём расписании: " . count($records) . "";
      
//   print_r($groups);
//      print_r($ctrl);
    
    if(isset($Message))
    {
        echo $Message;
    }
//     echo "No Message";
              /*
              Лекций:
              Практик:
              Лабораторных работ: // */
?>
</pre>      

      
      
<div class="container content">

    
        <u align=center><h4>Моё расписание <small><?php echo $ctrl->usertitle ?></small></h4></u>

<?php 
echo '<table class="table table-bordered table-striped">';

echo "<thead>
        <tr>
            <th><div align='right'>День &#9658;</div>&#9660;Час</th>
";
for( $day=0 ; $day<$days ; $day++ ) {
           echo "<td valign=top><b>". (1 + $day) ."</b></td>";
//            echo "<th style='vertical-align: top;'>". (1 + $day) ."</th>";
    }
    
echo "        </tr>
    </thead>
<tbody>
";
// for all positions
for( $pos=0 ; $pos<$daylessons ; $pos++ ) {
    
    echo "<tr>";

    // час
    echo "<td>" . (1+ $pos) . "</td>";
    
    for( $day=0 ; $day<$days ; $day++ ) {
    // for( $pos=0 ; $pos<$days * $daylessons ; $pos++ ) {

        echo "<td>";
        echo content_for($day * $daylessons + $pos);
        echo "</td>";
    }
    
    echo "</tr>";
}
    
echo "</tbody>
</table>";

?>
    
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>