<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Аудитории:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <link href="css/bootstrap-switch.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"> </script>
    <script src="js/bootstrap-switch.min.js"></script>
      
<!--       // see also https://ghinda.net/css-toggle-switch/bootstrap.html -->


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
         // подключаем общий фрагмент
         include_once "room_header.php";

        // SAVE
        $ctrl->saveToSESSION();
        // SAVE
  ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <small><a href="index.php">Домой</a></small></h3>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>

<!-- Навигация --><!-- 
<ul class="nav nav-pills">
  <li><a href="index.php">Домой</a></li>
  <li class="active"><a href="">Преподаватели</a></li>
  <li><a href="group.php">Группы и занятия</a></li>
  <li><a href="summary.php">Сводка</a></li>
  <li><a href="limit.php">Ограничения</a></li>
  <li><a href="calc.php">Расчёт</a></li>
  <li><a href="view.php">Просмотр</a></li>
</ul>      -->
<?php
    insert_navigation_pills("room");
?>
      
<pre><?php
     //// include_once "common/modals.html"; // подключаем общий фрагмент
      include_once "common.php"; // функции

    $records = PDOfetchAll("SELECT * FROM `room` WHERE curID=$ctrl->curID ORDER BY ID ASC");      
    
    if($roomID) {
        $record = $records[array_search($roomID, array_column($records, 'ID'))]; 
        $recname = $record['name']; 
    }
              
    if(isset($Message))
    {
        echo $Message;
    }
   //echo "\$roomID=".$roomID;
    //// $key = array_search(40489, array_column($userdb, 'uid'));
    ///  print_r(array_column($records, 'name'));
              
    ///  print_r(array_search($roomID, array_column($records, 'ID')));
    // print_r ($nestedrecs);
?></pre>
      
<h3> 
      
<ol class="breadcrumb">
  <li><a href="room.php">Аудитории</a></li>
  <?php if($roomID) { ?>
    <li class="active">
        <a href="room.php?r=<?php echo $roomID ?>"
        ><?php echo $recname ?></a></li>
    <?php } ?>
</ol>      
      
</h3>
      
      
      <script>
          /* switch */
          var chks = {};
          var chks_on = {};
          var waitingForInitToggle = true;
          var timeoutToggleTriggeredTimer = null;
          var toggleStatesChanges = {};
       try{
          setTimeout("waitingForInitToggle = false;", 1000);
           
          function switchChanged(event, state){
              if(!waitingForInitToggle)
              {
                  var room_id = event.target.attributes["roomID"].value;
//                   alert(room_id+' change to: '+state+' ! '/*+JSON.stringify(arguments)*/);// var myNewState = state.value;
                  
                  if(timeoutToggleTriggeredTimer)
                  {
                      clearTimeout(timeoutToggleTriggeredTimer);
                  }
                  timeoutToggleTriggeredTimer = setTimeout("saveAllChanges();", 800);
                  // save changes to map
                  toggleStatesChanges[room_id] = state;
              }
          }
          
          function saveAllChanges()
          {
              var str = "";
              for(var room_id in toggleStatesChanges) {
                  if(str != "")
                      str += ",";
                  
                  str += room_id + "-" + (toggleStatesChanges[ room_id ]? '1':'0');
              }
              
//               alert(str);
              window.location.href = "?<?php echo ($roomID? "r=$roomID&" : "") ?>toggle="+str;
          }
       } catch (e) {alert(e)}
          
      </script>

      
      <!-- Расположить таблицы рядом по горизонтали text-center -->
<div class="container-fluid content">
  <div class="row">
  <div class="col-md-4  col-sm-4">
      <table class="table table-condensed table-hover" style="width:20%">
      <thead>
        <tr>
          <th>№</th>
          <th colspan=2>Аудитория</th>
          <td><small><i>Удалить</i></small></td>
        </tr>
      </thead>
      <tbody>
          
          
          <!-- При клике по ссылке выбранный room становится текущим -->
        <?php foreach ($records as $i => $rec) { ?>
        <tr <?php if(($roomID) and $roomID == $rec['ID']) echo 'class="success"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><a href="?r=<?php echo $rec['ID'] ?>"><?php echo $rec['name'] ?></a></td>
            
          <td>
<!--               Switch  -->
              <input type=checkbox id="sw<?php echo $i ?>" roomID="<?php echo $rec['ID'] ?>" data-size="small">
              <script>
                       var chk_id = "#sw<?php echo $i ?>";
    //                   $('input:checkbox').bootstrapSwitch();
                       chks[chk_id] = $(chk_id); // get with jQuery
                       var room_flags = "<?php echo $rec['flags'] ?>";
                       chks_on[chk_id] = room_flags.indexOf("off");
              </script>
<!--                 /Switch -->
          </td>
            
          <td><button type="button" class="close" aria-hidden="true" onClick="var d=confirm('Удалить аудиторию? \nНаименование аудитории: <?php echo $rec['name'] ?>');if(d){window.location.href = '?del=<?php echo $rec['ID'] ?>';}"><img src='img/del.png' title="Удалить" alt="Удалить"></button></td>
        </tr>
        <?php } ?>
          
        <tr>
          <th><img src="img/add.png" /></th>
            <td colspan="2"><i><button type="button" class="btn btn-default" onClick="var n=prompt('Краткое наименование аудитории (до 10 знаков):','В907');if(n){window.location.href = '?new='+n.slice(0,10);}">Создать аудиторию</button></i></td>
          <!-- <td></td> -->
        </tr>
          
      </tbody>
    </table>
 </div>

              <script> // UPDATE CHECKBOXES view and set INITIAL STATE after some time
                  for(var chk_id in chks)
                  {
                      chks[chk_id].bootstrapSwitch({
                            onText:  'Использ.', // Используется // &oplus; &otimes;
                            offText: 'Закрыта', // Не использовать danger
                            onColor:  'success',
                            offColor: 'warning',
                            onSwitchChange: switchChanged
                        });
                       
//                       chks[chk_id].trigger('click');
                  }
                  function _f(){
                      for(var chk_id in chks)
                          if(chks_on[chk_id])
                              chks[chk_id].trigger('click');
                  }
                  if( <?php echo ($roomID)? "true":"false" ?> ) {
                        _f();
                  } else {
                      setTimeout(_f, 400); // just so (:-P)
                  }
              </script>
      
      
    <?php if($roomID) { // выбранo ?>
<div class="col-md-5 col-sm-5" style="border-width: 2px;background-color: #BDD8F9;">
    
        <u><h4>Настройки аудитории</h4></u>
        <form method="POST" action="">
            <div class="form-group text-left">
            <input type="hidden" name="roomID" value="<?php echo $record['ID'] ?>">

        <p class="text-left indent">Наименование аудитории</p>
        <input type="text" name=name class="form-control indent" placeholder="имя" minlength="1" maxlength="10" value="<?php echo $record['name'] ?>" required>
                
<!--                 Types -->
                
        <br>
        <p class="text-left indent">Типы занятий, для которых аудитория используется:</p>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="lec" <?php if(strpos($record['flags'], "lec")!==false) echo 'checked'; ?>>
            Лекции
          </label>
        </div>               
        <div class="checkbox">
          <label>
            <input type="checkbox" name="prac" <?php if(strpos($record['flags'], "prac")!==false) echo 'checked'; ?>>
            Практики
          </label>
        </div>               
        <div class="checkbox">
          <label>
            <input type="checkbox" name="lab" <?php if(strpos($record['flags'], "lab")!==false) echo 'checked'; ?>>
            Лаб. работы
          </label>
        </div>               

            </div>
            <div class="logModal-footer">
                <input type="submit" class="btn btn-primary" value="Сохранить">
            </div>
        </form>
 </div>
    <?php } ?>

      
 </div>
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>