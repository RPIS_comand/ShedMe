<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Сводка:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
         // подключаем общий фрагмент
// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=summary.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 2; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
} // */      
      
      
  ?>
  </head>
  <body>
<?php
     include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <small><a href="index.php">Домой</a></small></h3>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>


<!-- Навигация -->
<?php
    insert_navigation_pills("summary");
?>
<!-- / Навигация -->


<pre><?php

    /// print_r("POST: ");    print_r($_POST);

//     $is_cur_altered = PDOfetch("SELECT altered FROM `curriculum` WHERE curID=$ctrl->curID")["altered"] != 0;

      // кол-во уроков
    $limits = PDOfetch("SELECT * FROM `limits` WHERE curID=$ctrl->curID");
      $daylessons = $limits["lessons"];      
            $days = $limits["days"];      
    $lessons_prof = min( $limits["lessons_prof"] , $daylessons );
    $lessons_stud = min( $limits["lessons_stud"] , $daylessons );
              
    //     if(empty($records)) {
    //         // добавить лимиты к плану
    //         $ok = PDOexec("INSERT INTO `limits` (curID) VALUES($ctrl->curID);");
    //         $records = PDOfetchAll("SELECT * FROM `limits` WHERE curID=$ctrl->curID");      
    //     }
        
    // первое попавшееся расписание без учёта рейтинга (см. view.php, calc.php)
//     $shedID = PDOfetch("SELECT ID FROM `schedule` WHERE curID=$ctrl->curID")["ID"];

//   print_r($shedID);
              
    $lplans = PDOfetchAll("SELECT  subjID,groupID,lec,prac,lab
        FROM lessonplan as lp WHERE EXISTS (SELECT ID FROM `group` as g WHERE curID=$ctrl->curID AND g.ID=lp.groupID);");
              
    $combs = PDOfetchAll("SELECT  subjID,groupID,lec
        FROM combinedlecture as cl,combination as cmb WHERE combID=cmb.ID AND EXISTS (SELECT ID FROM `group` as g WHERE curID=$ctrl->curID AND g.ID=cl.groupID);");
              
 $comblecs = PDOfetchAll("SELECT  subjID,lec
        FROM combination as cmb WHERE EXISTS (SELECT curID FROM `professor` as f,`subject` as s WHERE profID=f.ID AND curID=$ctrl->curID AND s.ID=cmb.subjID);");
              
    $subjs = PDOfetchAll("SELECT  ID,profID,name
        FROM subject WHERE EXISTS (SELECT ID FROM `professor` as f WHERE curID=$ctrl->curID AND f.ID=subject.profID);");
              
//     SELECT SUM(lec + prac + lab),COUNT(*) as cnt INTO @lp_lessons,@lp_cnt FROM lessonplan as lp WHERE EXISTS (SELECT ID FROM `group` as g WHERE curID=in_curID AND g.ID=lp.groupID);
//     SELECT SUM(lec) INTO @cmb_lessons FROM combination as cmb WHERE EXISTS (SELECT curID FROM `professor` as f,`subject` as s WHERE profID=f.ID AND curID=in_curID AND s.ID=cmb.subjID);
              
              
//   echo " Занятий учебном плане: " . count($records) . "";
      
    $groups = PDOfetchAll("SELECT * FROM `group` WHERE curID=$ctrl->curID");
    $profs  = PDOfetchAll("SELECT * FROM `professor` WHERE curID=$ctrl->curID");
/*
  print_r($lplans);
  print_r($combs);
  print_r($comblecs);
  print_r($subjs);
  print_r($groups);
  print_r($profs);
// */
              
//               =========================== CALC SUMMS ===========================
              
  $group_data = array();
   $prof_data = array();
  $table_data = array();

  foreach($groups as $r) {
     $group_data[ $r["ID"] ] = array("name" => $r["name"],"lec" => 0,"prac" => 0,"lab" => 0);
  }
  foreach($profs as $r) {
      $prof_data[ $r["ID"] ] = array("name" => $r["name"],"lec" => 0,"prac" => 0,"lab" => 0);
  }

function add_lesson_spec($key, $subjectName, $str_data) {
        global $table_data;
        if( ! isset($table_data[ $key ]) )
            $table_data[ $key ] = array();
        if( ! isset($table_data[ $key ][ $subjectName ]) )
            $table_data[ $key ][ $subjectName ] = "";
        else
            $table_data[ $key ][ $subjectName ] .= "<br>";

        $table_data[ $key ][ $subjectName ] .= $str_data;
}

  // $types  = array("lec","prac","lab");
  $typeHr = array("lec" => "лекц.","prac" => "практ.","lab" => "л/р");
      
              
foreach($subjs as $s) {
   $subjectName = $s["name"];
    
   foreach($lplans as $lp) {  // $subjs as $s

      if( $lp["subjID"] == $s["ID"] )
      {
        $str_data = ""; //// "<b>".$s["name"]."</b><br>";
		$total_lessons = 0;
		foreach( $typeHr as $key => $keyHr) {
			$lesson_count = $lp[$key];
			if($lesson_count > 0)
			{
				$total_lessons += $lesson_count;
				$str_data .= " <nobr><b>". $lesson_count ."</b> <i>". $keyHr ."</i></nobr>";
				$prof_data [ $s ["profID" ] ][$key] += $lesson_count;
				$group_data[ $lp["groupID"] ][$key] += $lesson_count;
			}
		}
		  
		// add to table_data
		if($total_lessons > 0)
		{
			$key = $s["profID"].'-'.$lp["groupID"];
            add_lesson_spec($key, $subjectName, $str_data);
// 			if( isset($table_data[ $key ]) )
// 				$table_data[ $key ] .= "<br>";
// 			else
// 				$table_data[ $key ] = "";
// 			$table_data[ $key ] .= $str_data;
		}
      }
   }
  foreach($comblecs as $c) {

      if( $c["subjID"] == $s["ID"] )
      {
		$lesson_count = $c["lec"];
		if($lesson_count > 0)
		{
			$prof_data [ $s ["profID" ] ] ["lec"] += $lesson_count;
		}

      }
  }
  foreach($combs as $c) {

      if( $c["subjID"] == $s["ID"] )
      {
		$lesson_count = $c["lec"];
		if($lesson_count > 0)
		{
			$str_data = "<nobr><b>". $lesson_count ."</b> <i>общ.лекц.</i></nobr> ";
			$group_data[ $c["groupID"] ] ["lec"] += $lesson_count;

			// add to table_data
			$key = $s["profID"].'-'.$c["groupID"];
            add_lesson_spec($key, $subjectName, $str_data);

// 			if( isset($table_data[ $key ]) )
// 				$table_data[ $key ] .= "<br>";
// 			else
// 				$table_data[ $key ] = "";
// 			$table_data[ $key ] .= $str_data;
		}
      }
  }
}
    
    if(isset($Message))
    {
        echo $Message;
    }
//     echo "No Message";
              /*
              Лекций:
              Практик:
              Лабораторных работ: // */
?>
</pre>      

      
      
<div class="container-fluid content">

    
        <u align=center><h4>Сводка для текущего учебного плана</h4></u>

<?php 
    
echo '<table class="table table-bordered">';

echo "<thead>
        <tr>
            <th></th>
";
    foreach($groups as $g) {
            echo "<th>". $g["name"] ."</th>";
    }
            //     Всего
echo "        <th>&sum;</th></tr>
    </thead>
<tbody>
";
    
// by all 
foreach($profs as $f) {

    echo "<tr>";
    echo "<th>" . $f["name"] . "</th>";
        
    foreach($groups as $g) {

        // get from table_data
        $str_data = "";
        $key = $f["ID"].'-'.$g["ID"];
        if( isset($table_data[ $key ]) )
        {
            //// echo $key;
            $str_data = "";
            foreach($table_data[ $key ] as $subjName => $spec) {
                $str_data .= (empty($str_data)? "":"<br>"). "<b>". $subjName ."</b><div  align=center>". $spec ."</div>";
            }
        }
        echo "<td>$str_data</td>";
    }
    // Сумма уроков преподавателя
        $str_data = "";
		$total_lessons = 0;
		foreach( $typeHr as $key => $keyHr) {
			$lesson_count = $prof_data[$f["ID"]][$key];
			if($lesson_count > 0)
			{
				$total_lessons += $lesson_count;
				$str_data .= "<nobr>". $lesson_count ." <i>". $keyHr ."</i></nobr><br>";
                /// (empty($str_data)? "":""). 
                        //             $days = $limits["days"];      
                        //     $lessons_prof = $limits["lessons_prof"];      
                        //     $lessons_stud = $limits["lessons_stud"];      
            }
		}
    if($total_lessons > $days*$lessons_prof)
        $str_data = "<td>". $str_data. "<font color=red><b>$total_lessons <i>всего</i></b></font></td>";
    else
        $str_data = "<td>". $str_data. "<b>$total_lessons <i>всего</i></b></td>";
    echo $str_data;
        
    echo "</tr>";
}

    // Сумма уроков группы
    echo "<tr>";
    echo "<th>&sum;</th>";
        
    foreach($groups as $g) {

//         echo "<td>$str_data</td>";
        $str_data = "";
		$total_lessons = 0;
		foreach( $typeHr as $key => $keyHr) {
			$lesson_count = $group_data[$g["ID"]][$key];
			if($lesson_count > 0)
			{
				$total_lessons += $lesson_count;
				$str_data .= "<nobr>". $lesson_count ." <i>". $keyHr ."</i></nobr><br>";
                /// (empty($str_data)? "":""). 
                        //             $days = $limits["days"];      
                        //     $lessons_prof = $limits["lessons_prof"];      
                        //     $lessons_stud = $limits["lessons_stud"];      
            }
		}
        if($total_lessons > $days*$lessons_stud)
            $str_data = "<td>". $str_data. "<font color=red><b>$total_lessons <i>всего</i></b></font></td>";
        else
            $str_data = "<td>". $str_data. "<b>$total_lessons <i>всего</i></b></td>";
        echo $str_data;
    }

        echo "<td></td>"; // &dot;
    
    echo "</tr>";

// ещё раз - заголовки групп
echo "<tr><th></th>";
    foreach($groups as $g) {
            echo "<th>". $g["name"] ."</th>";
    }
echo "<td></td></tr>";
    
echo "</tbody>
</table>";

?>
    
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>