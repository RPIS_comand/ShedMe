<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Расчёт:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
         // подключаем общий фрагмент
// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=calc.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 4; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
}

        // SAVE
        $ctrl->saveToSESSION();
        // SAVE
  ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <small><a href="index.php">Домой</a></small></h3>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>

<!-- Навигация -->
<?php
    insert_navigation_pills("calc");
?>
      
<pre><?php

    $is_cur_altered = PDOfetch("SELECT altered FROM `curriculum` WHERE ID=$ctrl->curID")["altered"] != 0;
//      print_r($is_cur_altered);
//     $is_cur_altered = $is_cur_altered;

              
              
    // первое попавшееся расписание без учёта рейтинга (см. view.php, calc.php)
    $shedID = PDOfetch("SELECT ID FROM `schedule` WHERE curID=$ctrl->curID")["ID"];
    
      if( is_null($shedID) ) // не существует...
      {
          PDOfetch("INSERT INTO `schedule` (curID,message) VALUES ($ctrl->curID,'no info')");
          
          $shedID = PDOfetch("SELECT ID FROM `schedule` WHERE curID=$ctrl->curID")["ID"];
      }
              
    $shed_details = PDOfetch("SELECT status,message FROM `schedule` WHERE ID=$shedID;");
    $shed_status = $shed_details["status"];
    $shed_status_mapHr = array("" => "есть проблемы","ok" => "OK","calc" => "<i>в процессе расчёта</i>");

//     print_r($shed_details);
//      print_r($is_cur_altered);
              
        
    if(isset($Message))
    {
        echo $Message;
    }
?></pre>
      
<div class="container-fluid content">
        <u><h3 align=center>Расчёт расписания для текущего учебного плана</h3></u>
    
<li>Состояние учебного плана: <b><?php if(!$is_cur_altered) echo "<i>не</i>" ?> изменён</b></li>
<li>Состояние расписания: <b><?php echo $shed_status_mapHr[$shed_status] ?></b></li>
<li>Перерасчёт требуется: <b><?php echo ($is_cur_altered or $shed_status=="")?"<i>да</i>":"нет" ?></b></li>
<h4>Сообщение расписания:</h4>
<pre><?php echo $shed_details["message"]?:"(пусто)" ?></pre>


<?php if($shed_status !== "calc") { ?>
    
<h2 align=center><a href="start_calc.php?shed=<?php echo $shedID ?>"><button><big>Рассчитать!</big></button></a><br><small>Внимание! Процесс может занять длительное время (минуты)</small></h2>
    
<?php } else { ?>

    <h2>Дождитесь окончания расчёта текущего расписания!<br><small><a href="progress.php">Наблюдайте за прогрессом</a></small></h2>
    <?php if(! isset($_SESSION["shed"])) { ?>
        Вероятно, вы не сможете просматривать прогресс выполнения, так как не Вы его запустили... <big>&#128513; ¯\_(ツ)_/¯</big>
        <p>
    <?php } ?>
            
<?php } ?>
            
      
  <li><a href="view.php">Просмотр итогов</a></li>
  <li><a href="viewlog.php" target=_blank>Открыть лог</a></li>  
    <p>
  <li>Ошибка? <a href="start_calc.php?shed=<?php echo $shedID ?>">Запустить принудительно</a></li>  
    <p>
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>