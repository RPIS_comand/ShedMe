DROP PROCEDURE IF EXISTS rank_pos;
### SEP
-- Алгоритм Ранжировать позицию для урока
CREATE PROCEDURE  rank_pos(IN  in_shedID INT UNSIGNED,
						   IN  in_posID INT UNSIGNED,
                           IN  in_type enum('lec', 'prac', 'lab') -- lesson type
                          )
		COMMENT "Ранжировать позицию для урока"
proc_block:BEGIN
    DECLARE in_pos INT SIGNED;
    DECLARE in_curID INT UNSIGNED;

      DECLARE done INT DEFAULT 0;
      DECLARE v_groupID INT UNSIGNED;
      /* DECLARE v_groupname VARCHAR(30); */
       DECLARE crsr_group CURSOR FOR SELECT ID/*,name*/ FROM `group` WHERE is_group_at_lesson(`group`.ID,@thislessonID);
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; -- /// SQLSTATE '02000'


    SELECT curID INTO in_curID FROM schedule WHERE ID = in_shedID;

    SELECT curID,lessonposID,pos
        INTO in_curID,@thislessonposID,in_pos
            FROM position WHERE ID = in_posID;

-- /// DBG
--  INSERT INTO debug (text) VALUES (CONCAT("Начало rank_pos(pos=",in_pos,"). this lessonposID=",@thislessonposID));
-- /// DBG

--  Если позиция уже запрещена
	IF @thislessonposID IS NULL THEN
-- /// DBG
 -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): pos is already forbidden."));
-- /// DBG
		LEAVE proc_block;
	END IF;
	


# !!! 
#     SELECT rooms INTO @maxrooms FROM limits WHERE curID = in_curID;
--  INSERT INTO debug (text) VALUES (@maxrooms);
 
 
     -- считать счётчик аудиторий
#     SELECT rooms INTO @Nlessons FROM position WHERE curID = in_curID AND pos = in_pos;
 -- INSERT INTO debug (text) VALUES (@Nlessons);
    
#     in_type
#     SELECT 
    SET @Nrooms =
        CASE  # in_type
          WHEN in_type = "lec" THEN   (SELECT rooms_lec   FROM position WHERE pos = in_pos)
          WHEN in_type = "prac" THEN  (SELECT rooms_prac  FROM position WHERE pos = in_pos)
          WHEN in_type = "lab" THEN   (SELECT rooms_lab   FROM position WHERE pos = in_pos)
        END;
#    ;    
    
    --  Если нет свободной аудитории
	IF (@Nrooms < 1) THEN
-- /// DBG
--   INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): ",IFNULL(@Nlessons,"None")," lessons are assigned to pos: ",in_pos," (max is ",@maxrooms,")"));
-- /// DBG
        UPDATE position SET lessonposID=NULL,score=-902 WHERE /*curID = in_curID AND*/ pos = in_pos;
        LEAVE proc_block;
	END IF;


    --  Получение группы, предмета и преподавателя
    -- @thislessonID, @thisprofID, @thisgroupID, @thissubjID, @thislessontype;
    SELECT lessonID INTO @thislessonID FROM lessonpos WHERE ID=@thislessonposID;

    SELECT lesson_get_prof(@thislessonID),lesson_get_subject(@thislessonID)
     INTO @thisprofID, @thissubjID;
     
-- /// DBG
-- INSERT INTO debug (text) VALUES (CONCAT("Начало rank_pos(pos=",in_pos,"). this lessonposID=",@thislessonposID," thislessonID=",@thislessonID," thisprofID=",@thisprofID," thissubjID=",@thissubjID,"."));
-- /// DBG

	/* 
	(+) Правила выбора подходящего места в расписании
	=============================================
	Выбранное место подходит, если:
	1. Преподаватель и сама группа не заняты
	2. В этот день у группы нет уроков или есть смежный урок (до или после)
	3. Есть свободная аудитория
	4. ...
	*/
    
	-- /*
	--  Если преподаватель занят
	IF is_prof_at_pos(in_shedID,@thisprofID,in_pos) THEN
        UPDATE position SET lessonposID=NULL,score=-8 WHERE curID = in_curID AND pos = in_pos;
-- /// DBG
 -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): ",IFNULL(@thislessonposID,"NULL"),"-id lpos has a busy prof."));
-- /// DBG
        LEAVE proc_block;
	END IF;

-- /// DBG
-- -- INSERT INTO debug (text) VALUES ("rank_pos(): преподаватель НЕ занят.");
-- /// DBG

# 	--  Если любая из групп-участниц занята
#     SELECT COUNT(ID) INTO @buzygroupID FROM `group`
#      WHERE curID=in_curID AND is_group_at_pos(in_shedID,`group`.ID,in_pos) LIMIT 1;
    
# 	IF @buzygroupID IS NOT NULL AND @buzygroupID > 0 THEN
#         UPDATE position SET lessonposID=NULL,score=-366 WHERE curID = in_curID AND pos = in_pos;
# -- /// DBG
#  -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): ",IFNULL(@thislessonposID,"NULL"),"-id lpos has a busy group."));
# -- /// DBG
#         LEAVE proc_block;
# 	END IF;

-- /// DBG
-- -- INSERT INTO debug (text) VALUES ("rank_pos(): НИ одна из групп-участниц НЕ занята.");
-- /// DBG

	SELECT lessons INTO @daylessons FROM limits WHERE curID = in_curID;
	--  Позиции начала дня и начала след. дня
	SET @daystart = in_pos - (in_pos % @daylessons);
	SET @dayend = @daystart + @daylessons - 1;  --  last on this day

-- /// DBG
 -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): day positions: ",@daystart," .. ",@dayend));
-- /// DBG

	--  Занятость преподавателя в этот день
	SELECT COUNT(*) INTO @Nlessons FROM position
     WHERE  curID = in_curID AND (pos BETWEEN @daystart AND @dayend) 
      AND is_prof_at_pos(in_shedID, pos, @thisprofID);
	SELECT lessons_prof INTO @maxlessons FROM limits WHERE curID = in_curID;
	--  Если день преподавателя занят
	IF (@Nlessons >= @maxlessons) THEN
		UPDATE position SET lessonposID=NULL,score=-888 WHERE curID = in_curID AND (pos BETWEEN @daystart AND @dayend);
-- /// DBG
 -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): too many (",IFNULL(@Nlessons,"None"),") lessons current prof has (max is ",@maxlessons,")"));
-- /// DBG
		LEAVE proc_block;
	END IF;


    --  Занятость групп в этот день 
    SELECT lessons_stud INTO @maxlessons FROM limits WHERE curID = in_curID;
 
 
# 	--  Если день любой из групп-участниц уже занят
#     SELECT COUNT(`group`.ID) INTO @buzygroupNumber FROM `group`
#      WHERE `group`.curID=in_curID 
# 	  AND @maxlessons <= ( -- IFNULL(
# 			SELECT COUNT(ID) FROM position 
# 			 WHERE curID = in_curID AND (pos BETWEEN @daystart AND @dayend)
# 			  AND is_group_at_pos(in_shedID,`group`.ID,pos)
# 							-- ,0)
# 						 )
# 	   LIMIT 1;
    
   
# 	IF (@buzygroupNumber IS NOT NULL AND @buzygroupNumber > 0) THEN  
# 		UPDATE position SET lessonposID=NULL,score=-966 WHERE curID = in_curID AND (pos BETWEEN @daystart AND @dayend);
# -- /// DBG
#  -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): ",IFNULL(@buzygroupNumber,"None")," groups are busy on this day. (max lessons for group: ",@maxlessons,")"));
# -- /// DBG
# 		LEAVE proc_block;
# 	END IF;

-- /// DBG
-- -- INSERT INTO debug (text) VALUES ("rank_pos(): НИ одна из групп-участниц НЕ загружена в этот день до предела.");
-- -- INSERT INTO debug (text) VALUES (CONCAT("daystart: ",@daystart,"\t dayend:",@dayend));
-- /// DBG

	-- ПО ВСЕМ ГРУППАМ --
	OPEN crsr_group;
	SET done = 0;
	-- /// DBG
-- INSERT INTO debug (text) VALUES ("crsr_group opened");
	-- /// DBG

	/* DECLARE v_groupID INT UNSIGNED; */
	/* DECLARE v_groupname VARCHAR(30); */

	-- Для каждой группы (v_lessonposID): проверка загруженности, смежного урока, вычисление рейтинга позиции по группе...
	
	SET @NgroupIterated = 0;
	SET @deltascore = 0.0;  -- Накопленные баллы всеми группами-участницами; нужно потом разделить на число групп
	SELECT `type` INTO @thislessontype FROM lesson WHERE ID=@thislessonID;
	
	REPEAT
        FETCH crsr_group INTO v_groupID /*,v_groupname*/ ;
        IF NOT done THEN
			-- /// DBG
             -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): starting iteration on group: ",v_groupID));
			-- /// DBG

			-- Подчёт числа уроков у группы в этот день расчёт Nlessons_group
			SELECT COUNT(ID),SUM(pos = in_pos) INTO @Nlessons_group,@busy_now
             FROM position 
			  WHERE curID = in_curID AND (pos BETWEEN @daystart AND @dayend)
			   AND is_group_at_pos(in_shedID, v_groupID ,pos);

-- /// DBG
-- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): A GROUP HAS ",IFNULL(@Nlessons_group,"None")," lessons, ",IFNULL(@busy_now,"None")," lessons NOW."));
-- /// DBG

             -- дублирование проверки, закомментированной выше  (дало прирост скорости!)
			IF @busy_now IS NOT NULL AND @busy_now > 0 THEN
				UPDATE position SET lessonposID=NULL,score=-366 WHERE curID = in_curID AND pos = in_pos;
				LEAVE proc_block;
			END IF;

			IF @Nlessons_group >= @maxlessons THEN
				UPDATE position SET lessonposID=NULL,score=-36699 WHERE curID = in_curID AND (pos BETWEEN @daystart AND @dayend);
                -- /// DBG
                --  INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): too many (",IFNULL(@Nlessons_group,"None"),") lessons a group has on this day. (max lessons: ",@maxlessons,")"));
                -- /// DBG
				LEAVE proc_block;
			END IF;

			-- /// DBG
         -- INSERT INTO debug (text) VALUES (CONCAT("crsr_group: check group ",v_groupID," (with ",@Nlessons_group," at day) for lessons near ",in_pos));
			-- -- INSERT INTO debug (text) VALUES (CONCAT("is_group_at_pos(in_shedID, v_groupID, in_pos + 1 = ",in_pos + 1,"): ",is_group_at_pos(in_shedID, v_groupID, in_pos + 1)));
			-- /// DBG
			
			--  В этот день у группы нет уроков или есть смежный урок (до или после)
			SET @lesson_near_pos = -11;
			
			-- Проверка смежного урока
			SET @valid = 0;
			
			IF(@Nlessons_group = 0) THEN
				SET @valid = 1;
			ELSE
				SET @beforePLUSafter = 0;
			
				--  урок ДО
				IF(in_pos > @daystart AND is_group_at_pos(in_shedID, v_groupID, in_pos - 1)) THEN
					SET @lesson_near_pos = in_pos - 1;
					SET @beforePLUSafter = @beforePLUSafter + 1;
				END IF;
				--  урок ПОСЛЕ
				IF(in_pos < @dayend AND is_group_at_pos(in_shedID, v_groupID, in_pos + 1)) THEN
					SET @lesson_near_pos = in_pos + 1;
					SET @beforePLUSafter = @beforePLUSafter + 1;
				END IF;
				
				IF @beforePLUSafter <=> 1 THEN
					SET @valid = 1;
				END IF;
			END IF;

			-- /// DBG
            -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): lesson_near_pos: ",@lesson_near_pos));
			-- /// DBG
			

			IF (NOT @valid) THEN
				UPDATE position SET lessonposID=NULL,score=-202 WHERE curID = in_curID AND pos = in_pos;
				-- /// DBG
				 -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): No valid neighbour lessonpos found"));
				-- /// DBG
				LEAVE proc_block;
			END IF;
					
			--  POS is @VALID, Рассчитаем баллы (score)
			/* 	(+) Приоритеты для занятий
				============================

				+ Лекции - в начале дня
				+ Практики - в конце дня (после лекций)
				+ Лаб. - по 2 подряд (то же самое занятие) (не 1 и не 3)

				Общие предпочтения
				============================
				+ Тот же предмет не должен повторяться в один день через промеуток
				+ Лучше, если первая и последняя пары свободны
			*/	
			
-- /// DBG
-- -- INSERT INTO debug (text) VALUES ("rank_pos(): before CASE.");
-- -- INSERT INTO debug (text) VALUES (CONCAT("@thislessontype : ",@thislessontype));
-- /// DBG

			--  Позиция лекции / практики в дне или Позиция лабы относительно соседних лаб
			CASE @thislessontype
				WHEN "lec" THEN
					--  начало дня
					--  Добавить очки
					SET @deltascore = @deltascore + (@daylessons - ABS(in_pos-@daystart-1.0));
					-- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): type: lec. score=",@deltascore));
				WHEN "prac" THEN
					--  конец дня
					--  Добавить очки
					SET @deltascore = @deltascore + (@daylessons - ABS(@dayend-in_pos-1.0));
					-- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): type: prac. score=",@deltascore));
				WHEN "lab" THEN
					--  2 одинаковые лабы подряд
		-- 			INT UNSIGNED tmp_subjID; 
					-- VARCHAR(10) @tmp_lessontype;
					-- /*
					-- CALL at_pos_by_group(in_shedID, @lesson_near_pos, @thisgroupID, @tmp_subjID, @tmp_lessontype);
					
					SET @secondlabExists = 0; -- is_subject_at_pos(in_shedID,@thissubjID,@lesson_near_pos);
					
					-- Тот же предмет, тип-лаба
					SELECT COUNT(ID) INTO @secondlablessons FROM lesson 
					 WHERE `type`="lab" AND is_lesson_at_pos(in_shedID,ID,@lesson_near_pos)
					  AND @thissubjID = lesson_get_subject(ID);

					SET @secondlabExists = @secondlablessons IS NOT NULL AND @secondlablessons > 0;


					IF(@secondlabExists) THEN
						--  Добавить очки
						SET @deltascore = @deltascore + (@daylessons*2);
                            -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): type: lab. 2nd lab! score=",@deltascore));

						--  Проверить, чтобы не было 3 лабы подряд
						SET @third_lesson_pos = @lesson_near_pos + (@lesson_near_pos-in_pos);

						-- Тот же предмет, тип-лаба
						IF (@third_lesson_pos BETWEEN @daystart AND @dayend) THEN
						
							SELECT COUNT(ID) INTO @third_labs FROM lesson 
							 WHERE `type`="lab" AND is_lesson_at_pos(in_shedID,ID,@third_lesson_pos)
							  AND @thissubjID = lesson_get_subject(ID);

							IF(@third_labs IS NOT NULL AND @third_labs>0) THEN
								--  Снять очки (в минус)
								SET @deltascore = @deltascore - (@daylessons*3);
                                -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): type: lab. 3rd lab! score=",@deltascore));
							END IF;
						END IF;
					END IF;
					-- */
					-- SELECT 1;
-- /// DBG
				ELSE
                    SELECT 1;
					-- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): @thislessontype is unknown: ",@thislessontype));
-- /// DBG
					
			END CASE;
			
-- /// DBG
-- INSERT INTO debug (text) VALUES ("rank_pos(): current point.");
-- /// DBG

			--  Тот же предмет не должен повторяться в один день через промежуток
		-- 	EXISTS ( 
			SELECT COUNT(lesson.ID) INTO @duplicatelessons FROM lessonpos as lp,lesson
			 WHERE lp.shedID = in_shedID AND lp.lessonID = lesson.ID
			  AND (lp.pos BETWEEN @daystart AND @dayend)
			  AND (lp.pos NOT BETWEEN in_pos-1 AND in_pos+1)
			  AND is_group_at_lesson(v_groupID,lesson.ID)
			  AND @thissubjID = lesson_get_subject(lesson.ID);

-- /// DBG
-- INSERT INTO debug (text) VALUES ("rank_pos(): current point 2.");
-- /// DBG

			IF @duplicatelessons IS NOT NULL AND @duplicatelessons > 0 THEN
				--  Снять очки
				SET @deltascore = @deltascore - (@daylessons*1);
				-- /// DBG
                -- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): предмет повторяется в один день через промежуток! score=",@deltascore));
				-- /// DBG
			END IF;
			
			SET @NgroupIterated = @NgroupIterated + 1;
        END IF;
	UNTIL done END REPEAT;

	CLOSE crsr_group;
	-- /// DBG
-- INSERT INTO debug (text) VALUES ("crsr_group closed");
	-- /// DBG


    -- Усреднить на кол-во групп
    SET @deltascore = @deltascore / IF(@NgroupIterated > 0, @NgroupIterated, 1);

    --  Записать score
    UPDATE position SET score=@deltascore WHERE curID = in_curID AND pos = in_pos;

-- /// DBG
-- INSERT INTO debug (text) VALUES (CONCAT("rank_pos(pos=",in_pos,"): ",@NgroupIterated," groups processed;  done=",done,"; score=",@deltascore," END."));
-- /// DBG
    
END;


### SEP --
-- DEBUG ONLY !
-- UPDATE position SET lessonposID=39 WHERE curID=20 -- ID=207;
### SEP
#  CALL rank_pos(2,221,'lec');
### SEP --
-- SELECT * FROM position WHERE curID=20 AND pos=0;  -- ID = 207
### SEP --
-- Костыль для вывода таблицы debug
