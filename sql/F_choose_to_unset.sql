DROP FUNCTION IF EXISTS choose_to_unset;
### SEP
-- Алгоритм Выбрать один из поставленных уроков так, чтобы он не образовал окна у студентов
CREATE FUNCTION choose_to_unset(in_shedID INT UNSIGNED)
				RETURNS INT UNSIGNED -- lessonposID
BEGIN
    /* DECLARE done INT DEFAULT 0; */
	DECLARE v_lessonposID,in_curID INT UNSIGNED;
	/* DECLARE crsr_pos CURSOR FOR SELECT ID FROM position WHERE curID=@thiscurID; */

	SELECT curID INTO in_curID FROM schedule WHERE ID = in_shedID;
	SELECT lessons INTO @daylessons FROM limits WHERE curID=in_curID;


    -- проверить наличие необъединённых (обычных) уроков
    SELECT COUNT(combID) INTO @normal_lessons FROM lessonpos,lesson
     WHERE shedID=in_shedID AND pos>=0 AND lessonID=lesson.ID AND combID IS NULL
      LIMIT 1;
    
    SET @normal_lessons = IFNULL(@normal_lessons, 0);

 -- INSERT INTO debug (text) VALUES (CONCAT("choose_to_unset(): normal_lessons=",@normal_lessons));
  --  RETURN NULL;
    
    
    -- Лимит на число итераций
    SET @countdown = 2 * (SELECT COUNT(*) FROM lessonpos WHERE shedID=in_shedID);

--  Ищем:
main_loop: REPEAT
    SET @countdown = @countdown - 1;

	SET @buzygroup_count = 1;
	
	--  выбрать случайным образом один из уроков
	SELECT ID,pos,lessonID INTO v_lessonposID,@current_pos,@current_lessonID FROM lessonpos
	 WHERE shedID=in_shedID AND pos>=0
	  ORDER BY RAND() LIMIT 1;

    IF (@normal_lessons > 0 AND @countdown >= 0) THEN
        -- отсеять общие лекции
        SELECT combID INTO @current_combID FROM lesson
         WHERE ID=@current_lessonID;
        IF (@current_combID IS NOT NULL) THEN
            ITERATE main_loop;
        END IF;
    END IF;
     
    -- ПОЗИЦИЯ ДО 
	IF (@current_pos % @daylessons)=0 THEN -- начало дня
		SET @buzygroup_count = 0;
	ELSE
		--  Если любая из групп занята
		SELECT COUNT(ID) INTO @buzygroup_count FROM `group`
		WHERE curID=in_curID AND is_group_at_pos(in_shedID,`group`.ID,@current_pos-1) LIMIT 1;
	END IF;

	IF @buzygroup_count = 0 THEN
		RETURN v_lessonposID;
	END IF;
	
	-- ПОЗИЦИЯ ПОСЛЕ
	IF (@current_pos % @daylessons)=(@daylessons-1) THEN -- конец дня
		SET @buzygroup_count = 0;
	ELSE
		--  Если любая из групп занята
		SELECT COUNT(ID) INTO @buzygroup_count FROM `group`
		WHERE curID=in_curID AND is_group_at_pos(in_shedID,`group`.ID,@current_pos+1) LIMIT 1;
	END IF;

	IF @buzygroup_count = 0 THEN
		RETURN v_lessonposID;
	END IF;
	
UNTIL @countdown<=0 END REPEAT main_loop;

 RETURN NULL;

END

### SEP
SELECT choose_to_unset(7);