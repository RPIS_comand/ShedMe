DROP PROCEDURE IF EXISTS make_lessons ;

### SEP
-- set character set latin1;
-- set names cp1251;
## SEP
-- delimiter ;
-- ### SEP

-- Алгоритм составления полного списка уроков
CREATE PROCEDURE make_lessons(IN  in_curID INT UNSIGNED,
                               OUT ok 		BOOLEAN,
                               OUT message 	BLOB)
		COMMENT 'Проверка выполнимости задачи и построение списка уроков'
BEGIN 
    DECLARE v_rooms INT;
    DECLARE v_days INT;
    DECLARE v_lessons INT;
    DECLARE v_lessons_prof INT;
    DECLARE v_lessons_stud INT;

      DECLARE done INT DEFAULT 0;
      DECLARE v_name CHAR(30);
      DECLARE v_profID INT UNSIGNED;
      DECLARE v_groupID INT UNSIGNED;
      DECLARE v_combID,v_lessplanID,v_lec,v_prac,v_lab INT UNSIGNED;
      
      DECLARE crsr_prof  CURSOR FOR SELECT ID,name FROM professor WHERE curID=in_curID;
      DECLARE crsr_group CURSOR FOR SELECT ID,name FROM `group` WHERE curID=in_curID;
      DECLARE crsr_comb  CURSOR FOR SELECT combination.ID,lec FROM `combination`,subject,professor WHERE curID=in_curID AND combination.subjID=subject.ID AND subject.profID=professor.ID;
      DECLARE crsr_lessplan  CURSOR FOR SELECT lessonplan.ID,lec,prac,lab FROM `lessonplan`,`group` WHERE curID=in_curID AND lessonplan.groupID=`group`.ID;
      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; -- /// SQLSTATE '02000'

proc_block: BEGIN 
	SET ok = 1;
	SET message = "make_lessons : ";
	
	SELECT lessons,lessons_prof,lessons_stud,rooms,days INTO v_lessons,v_lessons_prof,v_lessons_stud,v_rooms,v_days FROM limits WHERE curID=in_curID;

 -- SET message = CONCAT(message ,v_days,v_lessons,v_lessons_prof,v_lessons_stud,v_rooms,"\n");

-- /// DBG
 INSERT INTO debug (text) VALUES (CONCAT("make_lessons(): Проверка ограничений ..."));
-- /// DBG



-- WHILE (SELECT COUNT(ID) FROM room WHERE curID=in_curID) < v_rooms DO
REPEAT
    SELECT COUNT(ID) INTO @room_count FROM room WHERE curID=in_curID;
-- /// DBG
-- INSERT INTO debug (text) VALUES (@room_count);
-- /// DBG
    IF @room_count < v_rooms THEN
        INSERT INTO room (curID,name) VALUES (in_curID,CONCAT("Ауд.", @room_count+1));
    END IF;
UNTIL @room_count >= v_rooms END REPEAT;


-- /// DBG
INSERT INTO debug (text) VALUES (CONCAT("make_lessons(): Добавлены недостающие аудитории. Всего: ", @room_count));
-- /// DBG

-- /// DBG
# INSERT INTO debug (text) VALUES (CONCAT("make_lessons(): TODO: ВКЛ/ВЫКЛ аудитории.!"));
-- /// DBG


	--  аудиторий хватает на все занятия?
    
    -- Посчитать доступные в аудиториях
    SELECT 
         SUM(FIND_IN_SET("lec", flags))
        ,SUM(FIND_IN_SET("prac", flags))
        ,SUM(FIND_IN_SET("lab", flags))
        ,COUNT(ID)
     INTO @max_lec,@max_prac,@max_lab,v_rooms
      FROM room 
       WHERE curID=in_curID AND NOT FIND_IN_SET("off", flags);


    SET @total_positions = v_days * v_lessons;
    SET @max_lessons = v_rooms * @total_positions;
    
   SET @max_lec = @total_positions * IFNULL(@max_lec,0); 
   SET @max_prac= @total_positions * IFNULL(@max_prac,0); 
   SET @max_lab = @total_positions * IFNULL(@max_lab,0); 
    
	SET @total_lessons = 0, /*@lp_lessons = 0,*/ @cmb_lessons = 0; 
    
    SELECT SUM(lec),SUM(prac),SUM(lab),COUNT(ID) as cnt
     INTO @lp_lec,@lp_prac,@lp_lab,@lp_cnt
      FROM lessonplan as lp
       WHERE EXISTS 
           (SELECT ID FROM `group` as g WHERE curID=in_curID AND g.ID=lp.groupID);
           
    SELECT SUM(lec)
     INTO @cmb_lessons
      FROM combination as cmb
       WHERE EXISTS
            (SELECT curID FROM `professor` as f,`subject` as s WHERE profID=f.ID AND curID=in_curID AND s.ID=cmb.subjID);
    
   SET @total_lec = IFNULL(@lp_lec,0) + IFNULL(@cmb_lessons,0); 
   SET @total_prac= IFNULL(@lp_prac,0); 
   SET @total_lab = IFNULL(@lp_lab,0); 
   SET @total_lessons = @total_lec + @total_prac + @total_lab; 
   
    -- + (SUM lec FROM combination)
    -- WHERE EXISTS (SELECT ID FROM professor WHERE curID=$ctrl->curID AND professor.ID=lesson_get_prof(l.ID))
    
	SET ok = @total_lessons <= @max_lessons;
	 IF( @total_lec > @max_lec ) THEN  -- TRUE OR 
        SET ok = 0;
        SET message = CONCAT(message, "Не хватает аудиторий на все лекции. Требуется провести лекций: ",@total_lec,", \nВозможно провести лекций в неделю: ",@max_lec,".\n");
	END IF; 
	 IF( @total_prac > @max_prac ) THEN  -- TRUE OR 
        SET ok = 0;
        SET message = CONCAT(message, "Не хватает аудиторий на все практики. Требуется провести практик: ",@total_prac,", \nВозможно провести практик в неделю: ",@max_prac,".\n");
	END IF; 
	 IF( @total_lab > @max_lab ) THEN  -- TRUE OR 
        SET ok = 0;
        SET message = CONCAT(message, "Не хватает аудиторий на все л/р. Требуется провести л/р: ",@total_lab,", \nВозможно провести л/р в неделю: ",@max_lab,".\n");
	END IF; 

	 IF( NOT ok ) THEN  -- TRUE OR 
        LEAVE proc_block;
	END IF; 


-- /// DBG
 INSERT INTO debug (text) VALUES (CONCAT("Total lessons: ",@total_lessons,", max: ",@max_lessons));
-- /// DBG
    
	--  преподаватели не перегружены?
	SET @max_prof_lessons = v_days * IF(v_lessons<v_lessons_prof,v_lessons,v_lessons_prof); -- min of lessons

      OPEN crsr_prof;
      SET done = 0;

      -- Для каждого преподавателя (profID):
      REPEAT
        FETCH crsr_prof INTO v_profID, v_name;
        IF NOT done THEN
            SET @prof_lessons = 0, @lp_lessons = 0, @cmb_lessons = 0; 
            -- Посчитать число проводимых занятий
            SELECT SUM(lec + prac + lab),COUNT(*) as cnt INTO @lp_lessons,@lp_cnt FROM lessonplan as lp WHERE EXISTS (SELECT ID FROM `subject` as s WHERE profID=v_profID AND s.ID=lp.subjID);
            SELECT SUM(lec) INTO @cmb_lessons FROM combination as cmb WHERE EXISTS (SELECT ID FROM `subject` as s WHERE profID=v_profID AND s.ID=cmb.subjID);

           SET @prof_lessons = IFNULL(@lp_lessons,0) + IFNULL(@cmb_lessons,0);
           -- SET message = CONCAT(message,"\n",v_name,": ",@prof_lessons);

           -- SET ok = ok AND (@prof_lessons <= @max_prof_lessons); TRUE OR 
            IF ( NOT (@prof_lessons <= @max_prof_lessons) ) THEN 
                SET ok = FALSE;
                SET message = CONCAT(message, "Преподаватель ",v_name," должен провести ",@prof_lessons," занятий(-я) в неделю, что превышает максимальные ",@max_prof_lessons," занятий(-я). Необходимо сначала снизить нагрузку на этого преподавателя.\n");
            END IF;
        END IF;
      UNTIL done END REPEAT;

      CLOSE crsr_prof;

    IF( NOT ok ) THEN # Есть проблемы
        LEAVE proc_block;
    END IF; 
    
 -- SET message = CONCAT(message, "go Next...");
 
	--  группы не перегружены?
	SET @max_group_lessons = v_days * IF(v_lessons<v_lessons_stud,v_lessons,v_lessons_stud); -- min of lessons

      OPEN crsr_group;
      SET done = 0;

      -- Для каждой группы (groupID):
      REPEAT
        FETCH crsr_group INTO v_groupID, v_name;
        IF NOT done THEN
            SET @group_lessons = 0, @lp_lessons = 0, @cmb_lessons = 0; 
            -- Посчитать число проводимых занятий
            SELECT SUM(lec + prac + lab),COUNT(ID) as cnt INTO @lp_lessons,@lp_cnt FROM lessonplan as lp WHERE (v_groupID=lp.groupID);
            SELECT SUM(lec),COUNT(ID) INTO @cmb_lessons,@cmb_cnt FROM combination as cmb WHERE EXISTS (SELECT ID FROM `combinedlecture` as cl WHERE cl.groupID=v_groupID AND cl.combID=cmb.ID);

            
-- /// DBG
-- INSERT INTO debug (text) VALUES (CONCAT("group ",v_groupID," ",v_name));
-- /// DBG
 
           SET @lp_lessons = IFNULL(@lp_lessons,0);
           SET @cmb_lessons = IFNULL(@cmb_lessons,0);
           SET @group_lessons = @lp_lessons + @cmb_lessons;

           -- SET ok = ok AND (@prof_lessons <= @max_prof_lessons); TRUE OR 
            IF ( NOT (@group_lessons <= @max_group_lessons) ) THEN  --  Если превышает:
-- /// DBG
-- INSERT INTO debug (text) VALUES (v_name),(message);
-- /// DBG
                SET ok = FALSE;
                SET message = CONCAT(message, "Группа `",v_name,"`: ",@group_lessons," (",@lp_lessons,"+",@cmb_lessons," об.) занятий(-я) в нед. из ",@max_group_lessons," допустимых\n");
                --  Необходимо снизить нагрузку на ",v_name,".
                -- ;",@lp_cnt,",",@cmb_cnt,"
-- /// DBG
-- INSERT INTO debug (text) VALUES (message);
-- /// DBG
            END IF;
        END IF;
      UNTIL done END REPEAT;

      CLOSE crsr_group;

-- /// DBG
-- INSERT INTO debug (text) VALUES ("CLOSE crsr_group;");
-- /// DBG
 
    IF( NOT ok ) THEN # Есть проблемы
        LEAVE proc_block;
    END IF; 
	
-- /// DBG
-- INSERT INTO debug (text) VALUES (ok);
-- /// DBG
 
-- Проверить наличие всех записей временных позиций (position) для текущего уч.плана
   CALL reset_positions(in_curID);

	
--  Удалить все уроки текущего уч.плана
   DELETE FROM lesson WHERE EXISTS (SELECT ID FROM professor WHERE curID=in_curID AND professor.ID=lesson_get_prof(lesson.ID));
    
    
-- /// DBG
INSERT INTO debug (text) VALUES ("make_lessons(): Удалены все уроки текущего уч.плана");
-- /// DBG
 
--  Удалить все неиспользуемые Combination (оставшиеся после удаления групп)
    -- SELECT * FROM combination as cmb WHERE NOT EXISTS (SELECT combID FROM combinedlecture as cl WHERE cl.combID=cmb.ID);

DELETE FROM combination WHERE NOT EXISTS (SELECT combID FROM combinedlecture as cl WHERE cl.combID=combination.ID);

-- /// DBG
INSERT INTO debug (text) VALUES ("make_lessons(): Удалены все лишние комбинации текущего уч.плана");
-- /// DBG
 
--  По всем комбинированным лекциям CombinedLecture:
	--  Добавить урок (лек.) с пустой позицией (=-11) столько раз, сколько общих лекций должно быть проведено.
--      DECLARE v_combID,v_lessplanID,v_lec,v_prac,v_lab INT UNSIGNED;
      OPEN crsr_comb;
      SET done = 0;

-- /// DBG
-- INSERT INTO debug (text) VALUES ("Открыли crsr_comb");
-- /// DBG
 
      -- Для каждого объединения (v_combID):
      REPEAT
        FETCH crsr_comb INTO v_combID, v_lec;
        IF NOT done THEN
            SET @i = v_lec; -- , @lp_lessons = 0, @cmb_lessons = 0; 
            -- Перебрать лекции
          -- REPEAT
          -- UNTIL done END REPEAT;
          add_loop: LOOP
-- /// DBG
-- INSERT INTO debug (text) VALUES (CONCAT(v_combID," comb with ",v_lec," i=",@i));
-- /// DBG
             IF @i=0 THEN
                LEAVE add_loop;
             END IF;
             SET @i=@i-1;
             INSERT INTO lesson (lessplanID,combID,type) VALUES (NULL,v_combID, "lec");
          END LOOP add_loop;
        END IF;
      UNTIL done END REPEAT;

      CLOSE crsr_comb;

-- /// DBG
INSERT INTO debug (text) VALUES ("make_lessons(): Добавлены занятия-комбинации"); -- ("CLOSE crsr_comb;");
-- /// DBG

--  По всем записям плана занятий LessonPlan:
	--  Добавить урок всех нужных типов (лек,прак,лаб) с пустой позицией (=-11) столько раз, сколько в уч. плане для текущей группы.
      OPEN crsr_lessplan;
      SET done = 0;

-- /// DBG
-- INSERT INTO debug (text) VALUES ("Открыли crsr_lessplan");
-- /// DBG
 
      -- Для каждого плана занятия (lessplanID):
      REPEAT
        FETCH crsr_lessplan INTO v_lessplanID,v_lec,v_prac,v_lab;
        IF NOT done THEN
-- /// DBG
-- INSERT INTO debug (text) VALUES (CONCAT(v_lessplanID," lp with ",v_lec,v_prac,v_lab));
-- /// DBG
          SET @i = v_lec;
          add_loop: LOOP -- Перебрать лекции
             IF @i=0 THEN LEAVE add_loop; END IF;
             SET @i=@i-1;
             INSERT INTO lesson (lessplanID,combID,type) VALUES (v_lessplanID,NULL, "lec");
          END LOOP add_loop;
          
          SET @i = v_prac;
          add_loop: LOOP -- Перебрать практики
             IF @i=0 THEN LEAVE add_loop; END IF;
             SET @i=@i-1;
             INSERT INTO lesson (lessplanID,combID,type) VALUES (v_lessplanID,NULL, "prac");
          END LOOP add_loop;
          
          SET @i = v_lab;
          add_loop: LOOP -- Перебрать лабы
             IF @i=0 THEN LEAVE add_loop; END IF;
             SET @i=@i-1;
             INSERT INTO lesson (lessplanID,combID,type) VALUES (v_lessplanID,NULL, "lab");
          END LOOP add_loop;
        END IF;
      UNTIL done END REPEAT;

      CLOSE crsr_lessplan;

-- /// DBG
INSERT INTO debug (text) VALUES ("make_lessons(): Добавлены плановые занятия");
-- /// DBG


    --  Установить флаг curriculum.altered в false (признак того, что план был изменён и сопоставленные ему уроки более не верны)
    UPDATE curriculum SET altered=FALSE WHERE ID=in_curID;

    SET message = "Успех\n";

 END  proc_block; --   //
END; -- END
-- delimiter ;

-- set character set utf8;
-- set names utf8;
### SEP

-- CALL make_lessons(20, @isok, @msg);
-- SELECT isok, msg;

# SEP
/* SET @p0=3;CALL `make_lessons`(@p0, @p1, @p2); */
/* ### SEP */
/* SELECT @p0 AS `cur`, @p1 AS `ok`, @p2 AS `message`; */

