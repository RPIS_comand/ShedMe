-- DROP FUNCTION IF EXISTS find_pos;
### SEP
DROP PROCEDURE IF EXISTS Pfind_pos; -- procedure now
### SEP

-- Алгоритм Найти лучшее подходящее для урока место в расписании
CREATE PROCEDURE Pfind_pos(IN in_shedID INT UNSIGNED,
                           IN in_lessonposID INT UNSIGNED,
                           OUT out_bestpos INT)
# CREATE FUNCTION find_pos(in_shedID INT UNSIGNED,
# 						  in_lessonposID INT UNSIGNED)
# 				RETURNS INT
BEGIN
#     DECLARE v_bestpos INT DEFAULT -11;
    DECLARE v_posID, in_curID INT UNSIGNED;
    DECLARE in_type enum('lec', 'prac', 'lab');

    DECLARE done INT DEFAULT 0;
    DECLARE crsr_pos CURSOR FOR SELECT ID FROM position WHERE 1; -- curID=in_curID;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; -- /// SQLSTATE '02000'


    SELECT curID INTO in_curID FROM schedule WHERE ID = in_shedID;
    
    SET out_bestpos = NULL;

### DBG
-- INSERT INTO debug (text) VALUES ("find_pos(): Начало");
-- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): thiscurID=",in_curID,"; in_lessonposID=",in_lessonposID));
### DBG

    -- get lesson type
    SELECT type INTO in_type FROM lesson
        WHERE ID=(SELECT lessonID FROM lessonpos WHERE lessonpos.ID=in_lessonposID); 

        
    --  1) Сбросить временные данные таблицы position - перезаписать столбцы lessonposID и score всех строк (rooms оставить!)
    UPDATE position SET lessonposID=in_lessonposID,score=-9999 WHERE curID = in_curID;
    -- /// ,rooms=0
-- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): Сбросить временные данные таблицы position OK"));
    
	SELECT lessons INTO @daylessons FROM limits WHERE curID = in_curID;

    --  2) По всем pos /строкам\ таблицы position
    OPEN crsr_pos;
    SET done = 0;
    
    SET @sumOKscore = 0;

    --  2) По всем pos /строкам\ таблицы position
    repeat_loop:REPEAT
        FETCH crsr_pos INTO v_posID;
        IF NOT done THEN
            --  Рассчитать возможность и предпочтительность постановки урока в позицию
-- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): CALLing rank_pos on ",v_posID));
            ### здесь
            CALL rank_pos(in_shedID, v_posID, in_type);
            -- SELECT rank_pos(in_shedID, v_posID) INTO v_bestpos;
            ###
-- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): CALL rank_pos() on ",v_posID," OK"));
            -- SELECT 1 INTO out_bestpos;
            ###
            
            -- Отсеять, если уже достаточно найдено
            SET @OKscore = 0;
            
            SELECT score INTO @OKscore FROM position WHERE ID = v_posID AND (lessonposID IS NOT NULL) AND score > 0;
            -- Сбрасывается после SELECT !
            SET done = 0;
            
            IF @OKscore IS NOT NULL THEN
                SET @sumOKscore = @sumOKscore + @OKscore;
     -- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): sumOKscore = ",IFNULL(@sumOKscore,"000")));
                
                -- Если граница больше - расстановка будет лучше, но работать будет дольше
                IF @sumOKscore IS NOT NULL AND @sumOKscore > @daylessons THEN
     -- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): break ON sumOKscore = ",IFNULL(@sumOKscore,"000")));
                    LEAVE repeat_loop;
                    -- OR:
                    -- SET done = 1;
                END IF;
            END IF;
            
            -- Сбрасывается после SELECT !
            SET done = 0;
            
        END IF;
    UNTIL done END REPEAT;

    CLOSE crsr_pos;
 -- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): crsr_pos closed. done=",done));


-- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): before fetch best"));

	--  3) Выбрать позицию с максимальными баллами
    SELECT MAX(score) INTO @maxscore FROM position
     WHERE curID = in_curID AND lessonposID IS NOT NULL
      LIMIT 1;

### 
-- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): @maxscore: ",@maxscore));
     
    SELECT p.pos INTO out_bestpos FROM position AS p
     WHERE curID = in_curID AND lessonposID IS NOT NULL
      AND score >= @maxscore
       LIMIT 1;

###
 -- INSERT INTO debug (text) VALUES (CONCAT("find_pos(): bestpos: ",out_bestpos," Конец"));

#     RETURN v_bestpos; --  NULL if not Exists

END

### SEP
-- CALL Pfind_pos(2,1295,@best);
CALL Pfind_pos(7,2088,@best);

### SEP
SELECT @best;