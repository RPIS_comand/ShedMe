=============================================


F_find_pos.sql
=============================================

CREATE FUNCTION  find_pos(in_shedID INT UNSIGNED,
						  in_lessonposID INT UNSIGNED)
				RETURNS INT


P_rank_pos.sql
=============================================

CREATE PROCEDURE  rank_pos(IN  in_shedID INT UNSIGNED,
						   IN  in_posID INT UNSIGNED)
		COMMENT "Ранжировать позицию для урока"

-- Алгоритм Выбрать один из поставленных уроков так, чтобы он не образовал окна у студентов
CREATE FUNCTION choose_to_unset(in_shedID INT UNSIGNED)
				RETURNS INT UNSIGNED -- lessonposID


F_is_at.sql
=============================================

CREATE FUNCTION  is_group_at_lesson(in_groupID INT UNSIGNED, in_lessonID INT UNSIGNED)
	RETURNS BOOLEAN

CREATE FUNCTION is_group_at_pos(
				 in_shedID INT UNSIGNED,
				 in_groupID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN

CREATE FUNCTION is_lesson_at_pos(
				 in_shedID INT UNSIGNED,
				 in_lessonID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN

CREATE FUNCTION is_subject_at_pos(
				 in_shedID INT UNSIGNED,
				 in_subjID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN

CREATE FUNCTION is_prof_at_pos(
				 in_shedID INT UNSIGNED,
				 in_profID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN


F_lesson_get_prof.sql
=============================================

CREATE FUNCTION  lesson_get_prof(in_lessonID INT UNSIGNED)
	RETURNS INT UNSIGNED


F_lesson_get_subject.sql
=============================================

CREATE FUNCTION  lesson_get_subject(in_lessonID INT UNSIGNED)
	RETURNS INT UNSIGNED


F_queue.sql
=============================================

CREATE PROCEDURE  queue_push_back(IN  in_shedID INT UNSIGNED,
                             IN  in_lessonposID INT UNSIGNED)
		COMMENT "Добавить урок в конец очереди"

CREATE PROCEDURE  queue_push_front(IN  in_shedID INT UNSIGNED,
                             IN  in_lessonposID INT UNSIGNED)
		COMMENT "Добавить урок в начало очереди"

CREATE FUNCTION  queue_top(in_shedID INT UNSIGNED)
				RETURNS INT UNSIGNED -- OR NULL
		COMMENT "Получить верхушку очереди"


P_make_lessons.sql
=============================================

-- Алгоритм составления полного списка уроков
CREATE PROCEDURE make_lessons(IN  in_curID INT UNSIGNED,
                               OUT ok 		BOOLEAN,
                               OUT message 	BLOB)
		COMMENT 'Проверка выполнимости задачи и построение списка уроков'


P_make_schedule.sql
=============================================

CREATE PROCEDURE make_schedule(IN  in_shedID INT UNSIGNED,
                               OUT ok 		BOOLEAN,
                               OUT message 	BLOB)
		COMMENT 'Составление экземпляра расписания'


P_reset_positions.sql
=============================================

CREATE PROCEDURE reset_positions (IN in_curID  INT UNSIGNED)
