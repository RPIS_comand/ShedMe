DROP FUNCTION IF EXISTS choose_room;
### SEP


CREATE FUNCTION choose_room(in_lposID INT UNSIGNED)
				RETURNS INT UNSIGNED
		COMMENT "Выбрать ауд. для размещённого урока: ID или NULL если нечего"
BEGIN
    DECLARE in_curID,in_shedID,in_lessonID,out_roomID INT UNSIGNED; -- названы как везде, хоть и не IN
    DECLARE in_type enum('lec', 'prac', 'lab');
    DECLARE in_pos INT;
#     DECLARE v_days INT;
#     DECLARE v_lessons INT;
#     DECLARE v_lessons_prof INT;
#     DECLARE v_lessons_stud INT;

-- Курсоры
#     DECLARE done INT DEFAULT 0;
#     DECLARE v_roomID INT UNSIGNED;
#     DECLARE crsr_rooms CURSOR 
#      FOR SELECT ID FROM room as r
#       WHERE curID=in_curID 
#        AND NOT EXISTS(SELECT ID FROM lessonpos as l
#                    WHERE  r.ID=l.roomID AND pos=in_pos) ORDER BY ntypes ASC;
#     DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; -- /// SQLSTATE '02000'



    SELECT shedID,lessonID,pos INTO in_shedID,in_lessonID,in_pos
     FROM lessonpos WHERE ID=in_lposID;
     
    SELECT curID INTO in_curID FROM schedule WHERE ID=in_shedID; 

    SELECT type INTO in_type FROM lesson WHERE ID=in_lessonID; 

-- /// DBG
# INSERT INTO debug (text) VALUES (CONCAT("Добавлены недостающие аудитории. Всего: ", @room_count));
-- /// DBG

SELECT ID INTO out_roomID FROM room as r
    WHERE r.curID=in_curID 
       AND FIND_IN_SET("off",r.flags) = 0  -- work here ... one more change ! and again. i`m gitlab! & Server`s changes!
       AND FIND_IN_SET(in_type,r.flags) > 0 
       AND NOT EXISTS(SELECT ID FROM lessonpos as l
           WHERE  r.ID=l.roomID AND l.pos=in_pos AND l.ID<>in_lposID) -- curID is not needed to check: cur is determined by roomID
               LIMIT 1;


    RETURN out_roomID;

END

### SEP
SELECT choose_room(1452);
# Test OK.