DROP PROCEDURE IF EXISTS queue_push_back ;

### SEP

/* Алгоритм Добавить в конец очереди */
CREATE PROCEDURE  queue_push_back(IN  in_shedID INT UNSIGNED,
                             IN  in_lessonposID INT UNSIGNED)
		COMMENT "Добавить урок в конец очереди"
BEGIN
	--  Сдвинуть все остальные в очереди
	UPDATE lessonpos SET pos=pos-1 WHERE shedID=in_shedID AND pos <= -100;
	
	--  Установить требуемый
	UPDATE lessonpos SET pos=-100 WHERE shedID=in_shedID AND ID = in_lessonposID;

END

### SEP

DROP PROCEDURE IF EXISTS queue_push_front ;

### SEP

/* Алгоритм Добавить в начало очереди */
CREATE PROCEDURE  queue_push_front(IN  in_shedID INT UNSIGNED,
                             IN  in_lessonposID INT UNSIGNED)
		COMMENT "Добавить урок в начало очереди"
BEGIN
	--  верх очереди (очередь растёт от -100 в отрицательном направлении)
	SELECT MIN(pos) INTO @queue_top FROM lessonpos WHERE shedID = in_shedID AND ID<>in_lessonposID;
	-- find minimum
    -- ### SELECT MIN(SELECT -99, @queue_top) INTO @queue_top;
    IF @queue_top > -99 THEN
        SET @queue_top=-99;
    END IF;
	
	--  Установить требуемый
	UPDATE lessonpos SET pos=@queue_top-1 WHERE ID = in_lessonposID;

END

### SEP

DROP FUNCTION IF EXISTS queue_top ;

### SEP

/* Алгоритм Получить верхушку очереди */
CREATE FUNCTION  queue_top(in_shedID INT UNSIGNED)
				RETURNS INT UNSIGNED
		COMMENT "Получить верхушку очереди"
BEGIN
    DECLARE r_ID INT UNSIGNED;
	--  верх очереди (очередь растёт от -100 в отрицательном направлении)
	SELECT MIN(pos) INTO @queue_top FROM lessonpos WHERE shedID = in_shedID;
	IF @queue_top > -100 THEN
		RETURN NULL; --  queue is empty
	ELSE 
		SELECT ID INTO r_ID FROM lessonpos WHERE shedID=in_shedID AND pos = @queue_top;
        RETURN r_ID;
	END IF;

END
