-- /*
DROP PROCEDURE IF EXISTS reset_positions ;

### SEP
-- delimiter //

CREATE PROCEDURE reset_positions (IN in_curID  INT UNSIGNED)

BEGIN
	DECLARE lessons INT UNSIGNED; -- total lessons per week
    
	SELECT (l.days * l.lessons) INTO lessons FROM limits l WHERE l.curID = in_curID ;
    
### INSERT INTO debug (text) VALUES (CONCAT("reset_positions(): всего позиций: ",lessons));

-- получение числа записей во вспомог. таблице position,
    -- которые относятся к текущему уч.плану... НЕТ, ВСЕХ!
    SELECT COUNT(*) INTO @Nexists FROM position WHERE 1;--  curID = in_curID ;
    
    IF lessons != @Nexists 
            OR in_curID <> (SELECT curID FROM position LIMIT 1) -- вдруг это был другой план с таким же числом позиций
        THEN
        -- стирание вообще всех
###  INSERT INTO debug (text) VALUES (CONCAT("reset_positions(): стирание вообще всех: было ",@Nexists));
        TRUNCATE TABLE position;
        -- DELETE FROM position WHERE position.curID = in_curID;
        
		SET @i = 0;
        -- вставка полного набора
        REPEAT
           INSERT INTO position (curID, pos, rooms)  # !!! TODO: remove rooms
                VALUES (in_curID, @i, 0);
           SET @i = @i + 1;
        UNTIL @i = lessons END REPEAT;
	ELSE
    -- сбросить все lessonposID в NULL
        UPDATE position SET lessonposID=NULL,score=0,rooms=0 WHERE 1;--  curID = in_curID;  # !!! TODO: remove rooms
    END IF;
    
    
    -- Получение числа аудиторий, доступных для типов занятия
    SELECT 
        SUM(FIND_IN_SET("lec",  flags) > 0), 
        SUM(FIND_IN_SET("prac", flags) > 0), 
        SUM(FIND_IN_SET("lab",  flags) > 0)
         INTO @N_lec, @N_prac, @N_lab 
            FROM room
                WHERE curID = in_curID AND FIND_IN_SET("off",  flags) = 0;

    SET 
      @N_lec   = IFNULL(@N_lec,  0),
      @N_prac  = IFNULL(@N_prac, 0),
      @N_lab   = IFNULL(@N_lab,  0);


    UPDATE position
        SET
          rooms_lec  = @N_lec,
          rooms_prac = @N_prac,
          rooms_lab  = @N_lab
            WHERE 1;
    
    
END -- // 

-- */
### SEP

CALL reset_positions( 3
    -- (SELECT ID FROM curriculum WHERE 1 LIMIT 1)
    ) ;




