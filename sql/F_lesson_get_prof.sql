DROP FUNCTION IF EXISTS lesson_get_prof;
### SEP

-- Алгоритм Получение преподавателя занятия
CREATE FUNCTION  lesson_get_prof(in_lessonID INT UNSIGNED)
	RETURNS INT UNSIGNED
BEGIN
    DECLARE r_id INT UNSIGNED;
    SELECT profID INTO r_id FROM subject WHERE ID = lesson_get_subject(in_lessonID);
    RETURN r_id;
END
