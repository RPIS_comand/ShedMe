DROP FUNCTION IF EXISTS lesson_get_subject ;

### SEP

-- DELIMITER //
CREATE FUNCTION  lesson_get_subject(in_lessonID INT UNSIGNED)
    RETURNS INT UNSIGNED
BEGIN
    DECLARE r_id INT UNSIGNED;
    SELECT lessplanID INTO @lp FROM lesson WHERE ID = in_lessonID;
    
    IF @lp IS NOT NULL THEN
        SELECT subjID INTO r_id FROM lessonplan WHERE ID=@lp;
    ELSE
        SELECT combID INTO @cmb FROM lesson WHERE ID=in_lessonID;

        IF @cmb IS NOT NULL THEN
            SELECT subjID INTO r_id FROM combination WHERE ID=@cmb;
        END IF; 
    END IF;
    
    
    RETURN r_id;
END
-- //
-- DELIMITER ;

### SEP

 -- SET @mystr = "ВКСД АСАД"; 
SET @mystr = "ВКСД АСАД"; 

### SEP

SELECT (@mystr := CONCAT(@mystr,"ВКС АСАД")) as txt;
-- a comment.sql */