
/* Функция не была создана! (оставлена для истории)
-- Функция неоднозначна: не может вернуть конкретное, если запрошена объединённая лекция
DROP FUNCTION IF EXISTS lesson_get_group;
# SEP

CREATE FUNCTION  lesson_get_group(in_lessonID INT UNSIGNED)
    RETURNS INT UNSIGNED
BEGIN
    DECLARE r_id INT UNSIGNED;
    SELECT lessplanID INTO @lp FROM lesson WHERE ID = in_lessonID;
    
    IF @lp IS NOT NULL THEN
        SELECT groupID INTO r_id FROM lessonplan WHERE ID=@lp;
    ELSE
        SELECT combID INTO @cmb FROM lesson WHERE ID=in_lessonID;

        IF @cmb IS NOT NULL THEN
            SELECT groupID INTO r_id FROM combination WHERE ID=@cmb;
        END IF; 
    END IF;
    
    
    RETURN r_id;
END   */

/* -- ГОТОВА !
DROP FUNCTION IF EXISTS is_group_at_lesson;
# SEP

CREATE FUNCTION  is_group_at_lesson(in_groupID INT UNSIGNED, in_lessonID INT UNSIGNED)
	RETURNS BOOLEAN
BEGIN
    DECLARE r_id INT UNSIGNED;
    SELECT lessplanID,combID INTO @lp,@cmb FROM lesson WHERE ID = in_lessonID;
    
    IF @lp IS NOT NULL THEN
        SELECT groupID INTO r_id FROM lessonplan WHERE ID=@lp AND groupID=in_groupID;
    ELSE 
        IF @cmb IS NOT NULL THEN
            SELECT groupID INTO r_id FROM combinedlecture WHERE combID=@cmb AND groupID=in_groupID;
        ELSE
            SET r_id = 123; -- not null
        END IF; 
    END IF;
    
    
    RETURN r_id IS NOT NULL;
END   -- */

/*
# SEP
SELECT is_group_at_lesson(13,1475),is_group_at_lesson(14,1475);
# SEP
SELECT is_group_at_lesson( 5,1475),is_group_at_lesson( 6,1475);
# SEP
SELECT is_group_at_lesson( 4,1475),is_group_at_lesson( 9,1475);
# SEP
SELECT is_group_at_lesson(13,1476),is_group_at_lesson(14,1476);
# SEP
SELECT is_group_at_lesson(13,1477),is_group_at_lesson(14,1477);
# SEP
SELECT is_group_at_lesson(13,1478),is_group_at_lesson(14,1478);
# SEP
SELECT is_group_at_lesson(13,1479),is_group_at_lesson(14,1479);
# SEP
SELECT is_group_at_lesson(13,1480),is_group_at_lesson(14,1480);
*/

### SEP

DROP FUNCTION IF EXISTS is_group_at_pos;
### SEP
CREATE FUNCTION is_group_at_pos(
				 in_shedID INT UNSIGNED,
				 in_groupID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN
BEGIN
    DECLARE r_id INT UNSIGNED;
    
    SELECT lesson.ID INTO r_id
     FROM lesson 
      WHERE is_lesson_at_pos(in_shedID,lesson.ID,in_pos)
       AND is_group_at_lesson(in_groupID,lesson.ID)
        LIMIT 1;
    
    RETURN r_id IS NOT NULL;
END   

### SEP
-- ГОТОВА !
DROP FUNCTION IF EXISTS is_lesson_at_pos;
### SEP
CREATE FUNCTION is_lesson_at_pos(
				 in_shedID INT UNSIGNED,
				 in_lessonID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN
BEGIN
    DECLARE r_id INT UNSIGNED;
    -- SELECT lessplanID,combID INTO @lp,@cmb FROM lesson WHERE ID = in_lessonID;
    
    SELECT lessonID INTO r_id FROM lessonpos WHERE shedID=in_shedID AND pos=in_pos AND lessonID=in_lessonID;
    
    RETURN r_id IS NOT NULL;
END   

### SEP
-- ГОТОВА !
DROP FUNCTION IF EXISTS is_subject_at_pos;
### SEP
CREATE FUNCTION is_subject_at_pos(
				 in_shedID INT UNSIGNED,
				 in_subjID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN
BEGIN
    DECLARE r_id INT UNSIGNED;
    -- SELECT lessplanID,combID INTO @lp,@cmb FROM lesson WHERE ID = in_lessonID;
    
    SELECT lesson.ID INTO r_id
     FROM lesson 
      WHERE is_lesson_at_pos(in_shedID,lesson.ID,in_pos)
       AND in_subjID=lesson_get_subject(lesson.ID)
        LIMIT 1;
    
    RETURN r_id IS NOT NULL;
END   

### SEP
-- ГОТОВА !
DROP FUNCTION IF EXISTS is_prof_at_pos;
### SEP
 -- in_groupID INT UNSIGNED, in_lessonID INT UNSIGNED)
CREATE FUNCTION is_prof_at_pos(
				 in_shedID INT UNSIGNED,
				 in_profID INT UNSIGNED,
				 in_pos INT)
	RETURNS BOOLEAN
BEGIN
    DECLARE r_id INT UNSIGNED;
    
    SELECT lesson.ID INTO r_id
     FROM lesson 
      WHERE is_lesson_at_pos(in_shedID,lesson.ID,in_pos)
       AND in_profID=lesson_get_prof(lesson.ID)
        LIMIT 1;
    
    RETURN r_id IS NOT NULL;
END   

-- Протестировать ..!

### SEP
/* -- DEBUG ONLY !!!!!
SET @p=20;
UPDATE `lessonpos` SET `pos`=@p:=@p+1 WHERE 1;

# SEP
SELECT is_prof_at_pos(2,41,22); -- 1

# SEP
SELECT is_prof_at_pos(2,45,22); -- 0

# SEP
SELECT is_group_at_pos(2,13,22); -- 1

# SEP
SELECT is_group_at_pos(2,14,22); -- 1

# SEP
SELECT is_group_at_pos(2,13,20); -- 1
 */