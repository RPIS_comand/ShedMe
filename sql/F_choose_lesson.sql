DROP FUNCTION IF EXISTS choose_lesson;
### SEP

-- Алгоритм Выбрать неразмещённый урок для постановки в расписание
CREATE FUNCTION choose_lesson(in_shedID INT UNSIGNED)
				RETURNS INT UNSIGNED
		COMMENT "Выбрать неразмещённый урок: ID или NULL если нечего"
BEGIN
	--  Выбрать первый урок из очереди (или случайным образом один из неразмещённых уроков, если очередь пуста)
	SELECT MIN(pos) INTO @spec_pos FROM lessonpos WHERE shedID = in_shedID;
    
	CASE 
		WHEN @spec_pos > -11 THEN --  нет неразмещённых
			RETURN NULL;
		WHEN @spec_pos <= -100 THEN --  Из очереди
			SELECT ID INTO @lposID FROM lessonpos WHERE pos = @spec_pos LIMIT 1;
            RETURN  @lposID;
        ELSE BEGIN END;
	END CASE;
	
    
     --  выбираем из неразмещённых: случайная позиция -> урок -> первый такой урок -> первая аналогичная позиция (возможно та же)

    SELECT ID,lessonID INTO @lessonposID,@lsnID
     FROM lessonpos
      WHERE shedID = in_shedID AND @spec_pos = pos
        ORDER BY RAND() LIMIT 1;
        
/*   не сработало... пытался переделать, но, похоже, без временной таблицы не обойтись
    SELECT ID,lessonID INTO @lessonposID,@lsnID
     FROM (SELECT ID,lessonID INTO @lessonposID,@lsnID
             FROM lessonpos
              WHERE shedID = in_shedID AND @spec_pos = pos) as l
         JOIN ( SELECT RAND() * (SELECT COUNT(ID) FROM l) AS max_id ) AS m
      WHERE l.ID >= m.max_id
#             AND shedID = in_shedID AND @spec_pos = pos
       ORDER BY l.ID ASC
        LIMIT 1; */
    
    
    -- Эти изощрения не учли, что ТОТ ЖЕ урок уже м.б. поставлен
/*    SELECT lessplanID INTO  @lpID FROM lesson WHERE ID = @lsnID;
    SELECT     combID INTO @cmbID FROM lesson WHERE ID = @lsnID;
    
    SELECT ID INTO @firstsuchlessonID FROM lesson
     WHERE lessplanID <=> @lpID AND combID <=> @cmbID
      ORDER BY ID LIMIT 1;
            
    SELECT ID INTO @lessonposID FROM lessonpos WHERE lessonID = @firstsuchlessonID; */

    RETURN @lessonposID;

END

### SEP
SELECT choose_lesson(1);


/*
# Эффективная замена ORDER BY RAND()

SELECT f.id FROM files f
	    JOIN ( SELECT RAND() * (SELECT MAX(id) FROM files) AS max_id ) AS m
	    WHERE f.id >= m.max_id
	    ORDER BY f.id ASC
	    LIMIT 1;
*/