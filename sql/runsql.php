<?php
/*
Запуск:
vds84.server-1.biz/site366/shedme/sql/runsql.php?case=

*/

/* ЗАМЕТКИ
Фреймворки для php:
 Yii
 LaraVel

Концепция ORM

PHP-асинхронный mysql-запрос
http://qaru.site/questions/1243055/php-asynchronous-mysql-query

*/

    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

        //  вся процедура работает на сессиях.
        session_start();
        require_once "../connection.php";
        include_once "../common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();


$filename = "";
$query = "";

$fetchqueryes = array();

// не забываем менять на текущий вариант или приписывать к адресу: runsql.php?case=8
$currentCASE = 8;
if(isset($_GET["case"])) { $currentCASE = +($_GET["case"]); echo "\$currentCASE: $currentCASE<br>"; }
echo "<head><title>RUN SQL:$currentCASE</title></head>";
// which file to load
switch($currentCASE)
{
    case 100:
        $filename = "P_make_schedule.sql"; // OK
        
//         SELECT COUNT(*) FROM `lessonpos` WHERE `shedID`=1 AND pos>=0
        
        /// DEBUG
//         PDOexec("TRUNCATE TABLE debug;");
//         $fetchqueryes[] = "SELECT text FROM debug ORDER BY ID;";
        /// DEBUG
        break;
    case 1:
        $filename = "P_make_lessons.sql";
        // $query = "";
        // Уроки тек. уч.плана: $fetchqueryes[] = "SELECT * FROM lesson as l WHERE EXISTS (SELECT ID FROM professor WHERE curID=$ctrl->curID AND professor.ID=lesson_get_prof(l.ID))"; 
        /// DEBUG
        PDOexec("TRUNCATE TABLE debug;");
        $fetchqueryes[] = "SELECT text FROM debug ORDER BY ID;";
        /// DEBUG
        break;
    case 2:     // OK !
        $filename = "F_lesson_get_subject.sql";
        //$fetchqueryes[] = "SELECT * FROM (SHOW FUNCTION STATUS) WHERE 1";
        $fetchqueryes[] = "SELECT lesson_get_subject(2);";
        $fetchqueryes[] = "SELECT lesson_get_subject(3);";
        //$fetchqueryes[] = 'SELECT "ВКСД АСАД" as txt;';
        $fetchqueryes[] = "SHOW FUNCTIONS IN `shedme`";
        break;
    case 3:      // OK !
        $filename = "F_lesson_get_prof.sql";
        $fetchqueryes[] = "SELECT lesson_get_subject(2);";
        $fetchqueryes[] = "SELECT lesson_get_subject(3);";
        $fetchqueryes[] = "SELECT lesson_get_prof(2);";
        $fetchqueryes[] = "SELECT lesson_get_prof(3);";
    case 4:      // OK !
        $filename = "P_reset_positions.sql";
        break;
    case 5:      // OK !??
        $filename = "F_queue.sql";
        break;
    case 6:      // OK !
        $filename = "F_choose_lesson.sql";
        // $query = "SELECT choose_lesson(2);";
        $fetchqueryes[] = "SELECT choose_lesson(2);";
        break;
    case 7:      // 
         $filename = "F_find_pos.sql";
        //// PDOexec("UPDATE lessonpos SET pos=NULL WHERE ID=33");
//         $fetchqueryes[] = "SET @best = find_pos(2,40);";
//         $fetchqueryes[] = "SELECT @best;";
//         $query = "SELECT * FROM position WHERE curID=20;";
            /// DEBUG
            PDOexec("TRUNCATE TABLE debug;");
            PDOexec("INSERT INTO debug (text) VALUES (\"Начало отладки\");");
            $fetchqueryes[] = "SELECT text FROM debug ORDER BY ID;";
            /// DEBUG
        break;
        
    case 8:      // 
        $filename = "P_rank_pos.sql";
//         $query = "SELECT 8+9;";
            /// DEBUG
            PDOexec("TRUNCATE TABLE debug;");
            PDOexec("INSERT INTO debug (text) VALUES (\"Начало отладки\");");
            /// DEBUG
        $fetchqueryes[] = "SELECT text FROM debug ORDER BY ID; /*Тестировать подробно!*/";
//         $fetchqueryes[] = "SELECT rank_pos(2,9);";
        break;
        
    case 9:      // OK !
         $filename = "F_is_at.sql";
//        $filename = "P_rank_pos.sql";
    ////          UPDATE `lessonpos` SET `pos`=22 WHERE 1
        
        // Следующая строка вызывает временное (на несколько часов) падение сервера Apache (ошибка 500)
//          $fetchqueryes[] = "SELECT is_prof_at_pos(2,41,22); -- 1"; // 
        
        // Следующие строки НЕ вызывают падение сервера Apache !
//          $fetchqueryes[] = "SELECT is_prof_at_pos(2,41,22); -- 1\n";
//          $fetchqueryes[] = "SELECT is_prof_at_pos(2,41,22); /* 1 */";
        
        
//          $fetchqueryes[] = "SELECT is_prof_at_pos(2,45,22); -- 0";
        break;
    case 10:      // 
         $filename = "F_choose_to_unset.sql";
        break;
    case 11:
        $filename = "F_choose_room.sql";
        break;
} // */ 


// get query from file
if (! empty($filename)) {
    $query = file_get_contents($filename);
//    $fetchqueryes[] = "SELECT \"$query\";";
    
    //$query .= "/* abc */ 
    //SELECT '$query' as txt;";

    /*
    echo "<pre>";
    print_r(htmlentities($query));
    echo "</pre><br>=======\n==========";
    // print_r("</pre><br>=======\n==========");
    // */

}
    print_r("<h2>Working on file <i><small>".((! empty($filename))?$filename:"-a query-")."</small></i></h2>");

// SPLIT Query by "### SEP"

$queryes = explode("### SEP\n", $query);


// run
    ///$ok = PDOfetchAll($query);
foreach($queryes as $i => $q)
{
    if(empty($q)) continue;
    
    echo "<h3>Query № $i</h3>";
    echo "<pre>";
    print_r(htmlentities($q));
    echo "</pre><br>=================";

    $sth = $dbhandle->prepare($q);
    $ok = $sth->execute();
    $res = $sth->fetchAll(PDO::FETCH_ASSOC);
    $info = $sth->errorInfo();

        echo "<br>Error info:";
    print_r($info);
        echo "<br>=================";

    echo "<br>&lt;Run status: &gt;";
    print_r($ok);
    //// print_r($sth);
    echo "&lt;/Run status&gt;<br>";
    echo "<br>&lt;Run result: &gt;<pre>";
    print_r($res);
    echo "</pre>&lt;/Run result&gt;<br>";
}

echo "<h2>Queryes from <i>runsql.php</i></h2>";

foreach($fetchqueryes as $q)
{
    print_r("<h3>Query: <small><i>$q</i></small> Result:</h3>");
    $result = PDOfetchAll($q);
    echo "<pre>";
    print_r($result?:'NULL RESULT');
    echo "</pre><br>=================";
}


echo "<br>END"
?>