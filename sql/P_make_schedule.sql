DROP PROCEDURE IF EXISTS make_schedule ;

### SEP

-- Алгоритм составления экземпляра расписания
CREATE PROCEDURE make_schedule(IN  in_shedID INT UNSIGNED,
                               OUT ok 		BOOLEAN,
                               OUT out_message 	BLOB)
		COMMENT 'Составление экземпляра расписания'
BEGIN 
    DECLARE in_curID INT UNSIGNED; -- назван как везде, хоть и не IN
    DECLARE v_rooms INT;
    DECLARE v_days INT;
    DECLARE v_lessons INT;
    DECLARE v_lessons_prof INT;
    DECLARE v_lessons_stud INT;

-- Курсоры
    DECLARE done INT DEFAULT 0;
    DECLARE v_lessonposID INT UNSIGNED;
    DECLARE crsr_lessoncmb CURSOR 
     FOR SELECT ID FROM lessonpos as lpos
      WHERE shedID=in_shedID 
       AND EXISTS(SELECT ID FROM lesson as l
                   WHERE  lpos.lessonID=l.ID AND lessplanID IS NULL AND combID IS NOT NULL);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1; -- /// SQLSTATE '02000'
      
proc_block: BEGIN 
	SET ok = 1;
	SET out_message = "Задача: Составление экземпляра расписания\n";

    -- Инициализировать индикацию расчёта
    UPDATE `schedule` SET status="calc", `message` = "Составление расписания начато..." WHERE ID=in_shedID;
    -- SELECT * FROM `schedule` WHERE FIND_IN_SET("ok", status )

	SELECT curID INTO in_curID FROM schedule WHERE ID=in_shedID; 
	
	SELECT lessons,lessons_prof,lessons_stud,rooms,days INTO v_lessons,v_lessons_prof,v_lessons_stud,v_rooms,v_days FROM limits WHERE curID=in_curID;

    -- /// DBG
                                            TRUNCATE TABLE debug;
    INSERT INTO debug (text) VALUES (CONCAT("START make_schedule() on shedID: ",in_shedID,", curID: ",in_curID,";"));
    -- /// DBG

	SELECT altered INTO @curAltered FROM curriculum WHERE ID=in_curID; 
    
--  Если полный список всех уроков для текущего уч. плана ещё не существует или уч. план изменён:
IF @curAltered THEN
	--  составить полный список всех уроков у всех групп (с повторами по типам занятий и кол-ву часов).
    -- /// DBG
    INSERT INTO debug (text) VALUES ("make_schedule(): Полный список всех уроков... ");
    -- /// DBG

	CALL make_lessons(in_curID, ok, @out_message);
    SET out_message = CONCAT(out_message, "Полный список всех уроков... ",@out_message);

    -- сделать копию уроко-позиций для нашего расписания
    DELETE FROM lessonpos WHERE shedID=in_shedID;
    
    --  составить полный список всех ПОЗИЦИЙ уроков у всех уроков
    INSERT INTO lessonpos (lessonID,shedID,roomID,pos)
     SELECT ID,in_shedID,NULL,-11
      FROM lesson as l
       WHERE EXISTS (SELECT ID FROM professor WHERE curID=in_curID AND professor.ID=lesson_get_prof(l.ID));

ELSE
    -- /// DBG
    INSERT INTO debug (text) VALUES ("make_schedule(): Уч.План НЕ был изменён");
    -- /// DBG

    -- пере-инициализировать полный список всех ПОЗИЦИЙ уроков у всех уроков
    UPDATE lessonpos SET roomID=NULL,pos=-11 WHERE shedID=in_shedID;
    
    -- Сбросить временные позиции
    CALL reset_positions(in_curID);
    
END IF;

    

    -- /// DBG
    -- INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): OK=",ok));
    -- /// DBG

    IF( NOT ok ) THEN # Есть проблемы
       -- Сохранить результаты
       UPDATE `schedule` SET status=IF(ok,"ok",""), `message` = out_message WHERE ID=in_shedID;
        LEAVE proc_block;
    END IF; 
    
    -- /// DBG
    -- INSERT INTO debug (text) VALUES ("after leave");
    -- /// DBG
 	
	
--  очистить очередь уроков на первоочередную постановку (далее просто очередь).
--  добавить в очередь общие лекции (урок типа CombinedLecture), если таковые имеются.
      OPEN crsr_lessoncmb;
      SET done = 0;
    -- /// DBG
    -- INSERT INTO debug (text) VALUES ("crsr_lessoncmb opened");
    -- /// DBG

      -- Для каждого урока (v_lessonposID):
      REPEAT
        FETCH crsr_lessoncmb INTO v_lessonposID;
        IF NOT done THEN
            -- Элемент случайности
            IF RAND() < 0.5 THEN
                CALL queue_push_back (in_shedID, v_lessonposID);
            ELSE
                CALL queue_push_front(in_shedID, v_lessonposID);
            END IF;
            --  UPDATE lessonpos SET pos=(SELECT MIN(pos) FROM lessonpos WHERE shedID=in_shedID)-1 WHERE shedID=in_shedID AND v_lessonposID=lessonID;
    -- /// DBG
     INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): ",v_lessonposID,"-id combined lesson queued"));
    -- /// DBG

        END IF;
      UNTIL done END REPEAT;

      CLOSE crsr_lessoncmb;
    

-- /*
SET @iterations = 0; -- прошло итераций
SET @max_iterations = CAST(v_rooms * v_days * v_lessons * 2 AS UNSIGNED INTEGER);
-- 18; -- 

 INSERT INTO debug (text) VALUES ("make_schedule(): Начало главного цикла.");
 
--  Пока есть неразмещённые уроки:
main_loop: LOOP
	IF @iterations > @max_iterations THEN
		SET ok = 0;
        SET out_message = CONCAT(out_message, "Не удалось составить непротиворечивое расписание. Произведено итераций: ",@iterations,".\nПопробуйте перезапустить расчёт, может быть повезёт.\nПодробности см. в логе.\n");
		-- out_message += "Не удалось составить непротиворечивое расписание за %1 итераций. Неразмещённые предметы в очереди: %2";
         INSERT INTO debug (text) VALUES ("make_schedule(): Неразмещённые занятия:");
         INSERT INTO debug (text) 
          SELECT CONCAT("lessonpos: ",ID," at pos: ",pos,"   (lesson ",lessonID,")") 
           FROM lessonpos WHERE shedID=in_shedID AND pos<0;

		LEAVE main_loop;
    END IF;
    

	--  Извлечь урок из очереди, а если очередь пуста, выбрать любой неразмещённый.
	SELECT choose_lesson(in_shedID) INTO @current_lposID;
    
    -- Выход из главного цикла
    IF @current_lposID IS NULL THEN
        SET ok = 1;
        LEAVE main_loop;
    END IF;
    
	SET @iterations = @iterations + 1;
    -- /// DBG
     INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): ИТЕРАЦИЯ № ",@iterations));
    -- /// DBG

 INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): Выбран урок ",@current_lposID));
 
 -- INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): Ищем позицию для ",@current_lposID," ..."));

    --  Найти подходящие для урока места в расписании (учитывая правила, ограничения и приоритеты).
-- SET @bestpos = find_pos(in_shedID, @current_lposID);
    CALL Pfind_pos(in_shedID, @current_lposID, @bestpos);

-- INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): Найдена pos: ",@bestpos));
 
    --  если подходящие для урока места найдены:
    IF @bestpos IS NOT NULL THEN
        --  поставить урок на первую из лучших позиций. (установить позицию урока (LessonPos) на выбранном месте в расписании (час, день недели).)
        UPDATE lessonpos SET pos=@bestpos WHERE ID=@current_lposID;
        
--  Выбрать И Установить Аудиторию, обновить счётчики в position
        SELECT choose_room(@current_lposID) INTO @current_roomID;
        UPDATE lessonpos SET roomID=@current_roomID WHERE ID=@current_lposID;

  INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): chosen room: ",@current_roomID," (",(SELECT name FROM room WHERE ID=@current_roomID),")"));
        
        SELECT flags INTO @room_flags FROM room WHERE ID=@current_roomID;
        SELECT FIND_IN_SET("lec", @room_flags) > 0 INTO @increment_lec;
        SELECT FIND_IN_SET("prac",@room_flags) > 0 INTO @increment_prac;
        SELECT FIND_IN_SET("lab", @room_flags) > 0 INTO @increment_lab;
        
/*         UPDATE position SET rooms=rooms+1 WHERE pos=@bestpos; -- curID is not checked */
        -- -1 счётчик аудиторий по типам : занята
        UPDATE position
            SET
              rooms_lec  = rooms_lec  - @increment_lec,
              rooms_prac = rooms_prac - @increment_prac,
              rooms_lab  = rooms_lab  - @increment_lab
                WHERE pos=@bestpos; -- curID is not checked
        
        -- SELECT ok;
        
 INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): Ставим урок ",@current_lposID," на pos: ",@bestpos));
 
    ELSE -- .. иначе:
    
-- INSERT INTO debug (text) VALUES ("make_schedule(): Снимаем урок...");
 
            -- снять урок с очереди (во избежание накрутки позиции верхушки очереди)
            -- устранено UPDATE lessonpos SET pos=-11 WHERE ID=@current_lposID;
        --  поставить урок в начало очереди.
        CALL queue_push_front(in_shedID, @current_lposID);
        
        --  выбрать случайным образом и снять один из поставленных уроков так, чтобы он не образовал окна у студентов.
        SET @lpos_to_unset = choose_to_unset(in_shedID);


/*
    ОШИБКА ПРИ СНЯТИИ !!! Исправлено
*/


        IF @lpos_to_unset IS NULL THEN
            SET ok = 0;
            SET out_message = CONCAT(out_message, "Не удалось составить непротиворечивое расписание.\nОшибка при попытке снять занятие для постановки другого.\nПроверьте ограничения и попробуйте перезапустить расчёт.\nПроизведено итераций: ",@iterations,".\nПодробности см. в логе.\n");
             INSERT INTO debug (text) VALUES ("make_schedule(): Неразмещённые занятия:");
             INSERT INTO debug (text) 
              SELECT CONCAT("lessonpos: ",ID," at pos: ",pos,"   (lesson ",lessonID,")") 
               FROM lessonpos WHERE shedID=in_shedID AND pos<0;

           LEAVE main_loop;
        END IF;
    
        
        -- Откатить счётчик итераций
        SET @iterations = @iterations - 1;
        
 INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): Снимаем урок ",@lpos_to_unset," - в конец очереди."));
 
#  print position content
/*     INSERT INTO debug (text) 
      SELECT CONCAT("[position: ",p.ID,"] pos: ",p.pos,", (lessonpos ",IFNULL(p.lessonposID,"NULL"),"), score: ",p.score) 
       FROM position as p WHERE 1; */
       
#        ### !!!
#        LEAVE main_loop;
         
-- !!!!  Установить Аудиторию, обновить счётчики в position
        SELECT roomID INTO @room_to_unset FROM lessonpos WHERE ID=@lpos_to_unset;
        
#  INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): room_to_unset = ",@room_to_unset));
 
        UPDATE lessonpos SET roomID=NULL WHERE ID=@lpos_to_unset;
        
        SELECT flags INTO @room_flags FROM room WHERE ID=@room_to_unset;
        SELECT FIND_IN_SET("lec", @room_flags) > 0 INTO @increment_lec;
        SELECT FIND_IN_SET("prac",@room_flags) > 0 INTO @increment_prac;
        SELECT FIND_IN_SET("lab", @room_flags) > 0 INTO @increment_lab;
        
#  INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): room_flags = ",@room_flags));

/*         UPDATE position SET rooms=rooms+1 WHERE pos=@bestpos; -- curID is not checked */
        -- +1 счётчик аудиторий по типам : освобождена
        UPDATE position
            SET
              rooms_lec  = rooms_lec  + @increment_lec,
              rooms_prac = rooms_prac + @increment_prac,
              rooms_lab  = rooms_lab  + @increment_lab
                WHERE pos=(SELECT pos FROM lessonpos WHERE ID=@lpos_to_unset); -- curID is not checked
        
/*         -- -1 счётчик аудиторий
        UPDATE position SET rooms=rooms-1
         WHERE pos=(SELECT pos FROM lessonpos WHERE ID=@lpos_to_unset); -- curID is not checked */
        
        
        
        --  добавить снятый урок в конец очереди (это уберёт его из расписания).
        -- Без защиты от невозможности... (т.е. NULL)
        CALL queue_push_back(in_shedID, @lpos_to_unset);

#  INSERT INTO debug (text) VALUES (CONCAT("make_schedule(): Сняли урок ",@lpos_to_unset,"."));
         
    END IF;
END LOOP main_loop;
    
 INSERT INTO debug (text) VALUES ("make_schedule(): Конец главного цикла.");
 
    
    IF ok THEN     -- УСПЕХ
        SET out_message = CONCAT(out_message, "Расписание составлено. Произведено итераций: ",@iterations,".\n");
    END IF;
   -- */
   
   UPDATE `schedule` SET status=IF(ok,"ok",""), `message` = out_message WHERE ID=in_shedID;
   
    -- ###################### Здесь я остановился ######################
--  Посчитать рейтинг расписания.	(см. Алгоритм ниже)

INSERT INTO debug (text) VALUES ("make_schedule(): message:");
INSERT INTO debug (text) VALUES (out_message);

    -- ### SET out_message = "Успех";

 END  proc_block; --   //
END; -- END

### SEP

# SET @p0=5;CALL `make_schedule`(@p0, @p1, @p2);
### SEP
# SELECT @p0 AS `shed`, @p1 AS `ok`, @p2 AS `out_message`;
