<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Просмотр:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
      // Режим
      $isAdmin = $ctrl->usertype == "admin";
      
         // подключаем общий фрагмент
// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=view.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 2; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
} // */      
      
      
  ?>
  </head>
  <body>
<?php
     include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> 
      <?php if($isAdmin) { ?>
            <small><a href="index.php">Домой</a></small>
      <?php } ?>
</h3>
      <?php if($isAdmin) { ?>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>
      <?php } ?>


<!-- Навигация -->
<ul class="nav nav-pills">

<?php if($isAdmin) { 
    
    insert_navigation_pills("view");
    
 } elseif( ! $isAdmin) { ?>

    <li class="active"><a href="view.php">Общее расписание</a></li>
    <li><a href="viewmy.php">Моё расписание</a></li>
      
<?php } ?>

</ul>
<!-- / Навигация -->


<pre><?php

    /// print_r("POST: ");    print_r($_POST);

    $is_cur_altered = PDOfetch("SELECT altered FROM `curriculum` WHERE curID=$ctrl->curID")["altered"] != 0;
//     print_r( $is_cur_altered );
//     print_r( "<<<" );
    if($is_cur_altered) 
    { ?><font color=red><H3>Внимание! После расчёта расписания учебный план был изменён.<br/><small>Возможно некорректное отображение данных</small></H3></font><?php 
    }
    
    // кол-во уроков
    $daylessons = PDOfetch("SELECT lessons FROM `limits` WHERE curID=$ctrl->curID")["lessons"];      
          $days = PDOfetch("SELECT    days FROM `limits` WHERE curID=$ctrl->curID")["days"];      
              
    //     if(empty($records)) {
    //         // добавить лимиты к плану
    //         $ok = PDOexec("INSERT INTO `limits` (curID) VALUES($ctrl->curID);");
    //         $records = PDOfetchAll("SELECT * FROM `limits` WHERE curID=$ctrl->curID");      
    //     }
        
    // первое попавшееся расписание без учёта рейтинга (см. view.php, calc.php)
    $shedID = PDOfetch("SELECT ID FROM `schedule` WHERE curID=$ctrl->curID")["ID"];

//   print_r($shedID);
              
    $records = PDOfetchAll("SELECT ID,lessonID,shedID,roomID,pos
        FROM `lessonpos` WHERE shedID=$shedID AND pos>=0");
              
//   print_r($records);
  echo " Занятий учебном плане: " . count($records) . "";
      
    $groups = PDOfetchAll("SELECT * FROM `group` WHERE curID=$ctrl->curID");
              
//   print_r($groups);
//      print_r($ctrl);
    
    if(isset($Message))
    {
        echo $Message;
    }
//     echo "No Message";
              /*
              Лекций:
              Практик:
              Лабораторных работ: // */
?>
</pre>      

      
      
<div class="container-fluid content">

    
        <u align=center><h4>Расписание для текущего учебного плана</h4></u>

<?php 
    $typeHr = array("lec" => "лекция","prac" => "практ.","lab" => "л/р");
    
echo '<table class="table table-bordered">';

echo "<thead>
        <tr>
            <th>День</th>
            <th>Час</th>
";
    foreach($groups as $g) {
            echo "<th>". $g["name"] ."</th>";
    }
    
echo "        </tr>
    </thead>
<tbody>
";
// by all positions
for( $pos=0 ; $pos<$days * $daylessons ; $pos++ ) {
    
    if($pos % $daylessons == 0) // начало дня
    {
        // add empty row
        echo "<tr class=info><td><td>";
        foreach($groups as $g) { echo "<td>"; }
        echo "</tr><tr>";
        
        // день
        echo '<td rowspan="'.$daylessons.'">' . (1+ intdiv($pos, $daylessons)) . "</td>";
    }
    else
    {
        echo "<tr>";
    }
    
    // час
    echo "<td>" . (1+ ($pos % $daylessons)) . "</td>";
    
    foreach($groups as $g)
    {
        echo "<td>"; //. $g["name"] ."</td>";
        foreach($records as $r) {
            if($pos == $r["pos"])
            {
               $is_group_at = PDOfetch("SELECT is_group_at_lesson(".$g["ID"].",".$r["lessonID"].") as isAt;")["isAt"];
                if($is_group_at)
                {
                    PDOexec("SELECT
                    lesson_get_subject(".$r["lessonID"]."), lesson_get_prof(".$r["lessonID"].")
                    INTO @tmp_subjID,@tmp_profID");
               $subjName = PDOfetch("SELECT name FROM subject WHERE ID=@tmp_subjID;")["name"];
               $profName = PDOfetch("SELECT name FROM professor WHERE ID=@tmp_profID;")["name"];
               $lessonType=PDOfetch("SELECT type FROM lesson WHERE ID=".$r["lessonID"].";")["type"];

               $roomName = PDOfetch("SELECT name FROM room WHERE ID=".$r["roomID"].";")["name"];
               
                   
                   $lessonType = $typeHr[$lessonType];
                    // ауд. и преп. в 2 строки
//                    $cell_content = "<b>$subjName</b> <small><i>$lessonType</i><div align='right'>$profName</div>$roomName</small>";
                    // ауд. и преп. в 1 строку, таблицей
                   $cell_content = "<b>$subjName</b> <small><i>$lessonType</i><table width=100%><tr><td><i>$roomName</i></td><td align='right'>$profName</td></tr></table></small>";
                    // ауд. и преп. в 1 строку, стилем float (+ ненужный отступ после абзаца - м.б. от <p>)
//                    $cell_content = "<b>$subjName</b> <small><i>$lessonType</i><div><p style=\"float:left;\">$roomName</i></p><p align='right'>$profName</p></div></small>";
                   echo $cell_content;
                }
            }
            //// break;
        }
        echo "</td>";
    }
    echo "</tr>";
}
// $result = PDOfetchAll("SELECT ID,time,text FROM debug ORDER BY ID;");
// foreach($result as $key => $row)
//     echo "    
//     <tr>
//         <td>" .($key+1). ":" .$row["ID"]. "</td>
//         <td>" .$row["time"]. "</td>
//         <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
//         <td><pre>" .$row["text"]. "</pre></td>
//     </tr>";

echo "</tbody>
</table>";

?>
    
<?php if($isAdmin) { ?>
    <li><a href="viewlog.php" target=_blank>Открыть лог</a></li>  
<?php } ?>
    
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>