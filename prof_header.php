<?php 

// Текущие - сброшены по умолчанию
$profID = false;
$subjID = false;

// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=prof.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 4; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
    
}

// выбрать 
if(isset($_GET['p']))
{
    $id = stripslashes($_GET['p']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
  // !!!  $ctrl->profID = $id;
    $profID = $id;
    
    //header('Refresh: 0; url=prof.php');
}
// выбрать 
if(isset($_GET['s']))
{
    $id = stripslashes($_GET['s']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
  // !!!  $ctrl->profID = $id;
    $subjID = $id;
    
    //header('Refresh: 0; url=prof.php');
}


// добавить 
if(isset($_GET['new']))
{
    $name = stripslashes($_GET['new']);
    $name = trim($name);
    $name = htmlspecialchars($name, ENT_QUOTES);
    
    PDOexec("INSERT INTO `professor` (curID,name) VALUES($ctrl->curID,'$name');");
    
    header('Refresh: 0; url=prof.php');
    exit("Adding new professor...");
}
// удалить 
elseif(isset($_GET['del']))
{
    $id = stripslashes($_GET['del']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `professor` WHERE curID=$ctrl->curID AND ID=$id;") )
        $ctrl->message = "Преподаватель удалён!";
    else
        $ctrl->message = "Ошибка удаления!"; // */
    
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    
    header('Refresh: 0; url=prof.php');
    exit("Deleting a professor...");
}


// добавить ВЛОЖЕННЫЙ ЭЛЕМЕНТ
if(isset($_GET['newnest']))
{
    $name = stripslashes($_GET['newnest']);
    $name = trim($name);
    $name = htmlspecialchars($name, ENT_QUOTES);
    
    PDOexec("INSERT INTO `subject` (profID,name) VALUES($profID,'$name');");
    
    $newid = PDOfetch("SELECT LAST_INSERT_ID() as newid FROM `subject`;")['newid'];
    
    // ! впоследстви убрать, пeренести
    // PDOexec("CALL curriculum_altered($ctrl->curID);");

    header("Refresh: 0; url=prof.php?p=$profID&s=$newid");
    unset($newid);
    exit("Adding new subject...");
}
// удалить ВЛОЖЕННЫЙ ЭЛЕМЕНТ
elseif(isset($_GET['delnest']))
{
    $id = stripslashes($_GET['delnest']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `subject` WHERE profID=$profID AND ID=$id;") )
    {
        $ctrl->message = "Предмет удалён!";
        // обновить дату
        PDOexec("CALL curriculum_altered($ctrl->curID);");
    }
    else
        $ctrl->message = "Ошибка удаления!"; // */
    
    header("Refresh: 0; url=prof.php?p=$profID");
    exit("Deleting a subject...");
}
// выбрать 
if($subjID && isset($_GET['course']))
{
    $n = stripslashes($_GET['course']);
    $n = trim($n);
    $n = htmlspecialchars($n, ENT_QUOTES);
    
    PDOexec("UPDATE `subject` SET course=$n WHERE profID=$profID AND ID=$subjID;");
    // if( PDOexec("DELETE FROM `subject` WHERE profID=$profID AND ID=$id;") )
    
    header("Refresh: 0; url=prof.php?p=$profID&s=$subjID");
    exit("Updating a subject`s course: $n...");
}


?>