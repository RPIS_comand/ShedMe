<?php 

// Текущие - сброшены по умолчанию
$roomID = false;

// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=room.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 1; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
    
}

// выбрать 
if(isset($_GET['r']))
{
    $id = stripslashes($_GET['r']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $roomID = $id;
    
    //header('Refresh: 0; url=room.php');
}

// добавить 
if(isset($_GET['new']))
{
    $name = stripslashes($_GET['new']);
    $name = trim($name);
    $name = htmlspecialchars($name, ENT_QUOTES);
    
    PDOexec("INSERT INTO `room` (curID,name) VALUES($ctrl->curID,'$name');");
    
    // обновить число аудиторий в лимитах
    PDOexec("UPDATE `limits` SET rooms=(SELECT COUNT(ID) FROM room WHERE room.curID=$ctrl->curID) WHERE curID=$ctrl->curID;");
    
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    
    header('Refresh: 0; url=room.php');
    exit("Adding new room \"$name\"...  ");
}
// удалить 
elseif(isset($_GET['del']))
{
    $id = stripslashes($_GET['del']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `room` WHERE curID=$ctrl->curID AND ID=$id;") )
        $ctrl->message = "Аудитория удалёна!";
    else
        $ctrl->message = "Ошибка удаления!"; // */
    
    
    // обновить число аудиторий в лимитах
    PDOexec("UPDATE `limits` SET rooms=(SELECT COUNT(ID) FROM room WHERE room.curID=$ctrl->curID) WHERE curID=$ctrl->curID;");

    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    
        // SAVE
        $ctrl->saveToSESSION();
        // SAVE
    
    header('Refresh: 0; url=room.php');
    exit("Deleting a room...");
}

// обновить ВКЛ/ВЫКЛ
if($ctrl->curID && isset($_GET['toggle']))
{
    $n = stripslashes($_GET['toggle']);
    $n = trim($n);
    $n = htmlspecialchars($n, ENT_QUOTES);
    
    foreach( explode(',',$n) as $room_state ) {
        list ($r_id, $state) = explode ('-', $room_state);
        
        if($state == '1')
        {
//             $setq = "((flags+0) & (~POW(2, FIND_IN_SET('off',flags))) )"; // remove "off"
            $setq = "(TRIM(BOTH ',' FROM REPLACE(CONCAT(',', flags, ','), ',off,', ',')))"; // remove "off"
//             TRIM(BOTH ',' FROM REPLACE(CONCAT(',', categories, ','), ',2,', ','))
            
            $setq = "$setq,flags=IF(flags='','prac',flags)";
        } else {
            $setq = "((flags+0) | POW(2, FIND_IN_SET('off',flags)))"; // add "off"
        } // (categories+0) & ~POW(2, FIND_IN_SET('2', categories))
        
        $totalq = "UPDATE `room` SET flags=$setq WHERE curID=$ctrl->curID AND ID=$r_id;";
            //  curID=$ctrl->curID AND
        
        if( 
            PDOexec($totalq)
            )
        {
            $ctrl->message = "Состояние аудиторий обновлено";
//             echo "Success($r_id -> $state) : $setq<BR>";
        }
        else
        {
            $ctrl->message = "Ошибка обновления аудиторий!"; // */
            echo "Error ($r_id -> $state) : <BR>$totalq<BR>";
        }
    }
    
        // обновить дату изменения уч.плана
        PDOexec("CALL curriculum_altered($ctrl->curID);");

        // SAVE
        $ctrl->saveToSESSION();
        // SAVE
    
    header("Refresh: 0; url=room.php".($roomID? "?r=$roomID" : "")); // ?r=$roomID
    exit("Updating toggled states: $n ...");
}


// обновить
if(isset($_POST['roomID']))
{
    $id = trim(stripslashes($_POST['roomID']));
    $id = htmlspecialchars($id, ENT_QUOTES);

    $name = trim(stripslashes($_POST['name']));
    $name = htmlspecialchars($name, ENT_QUOTES);

//     print_r("POST: ");    print_r($_POST);
    
    $lec =isset($_POST['lec']);
    $prac=isset($_POST['prac']);
    $lab =isset($_POST['lab']);
    
    $setq= ltrim(
                   ($lec ?  ",lec" : "")
                  .($prac?  ",prac" : "")
                  .($lab ?  ",lab" : "") 
             , ",") ;
    
    if( !$lec && !$prac && !$lab )
    {
        $setq = "off";
    }
    
    
        $totalq = "UPDATE `room` SET name=\"$name\", flags=\"$setq\" WHERE curID=$ctrl->curID AND ID=$id;";
            //  curID=$ctrl->curID AND
        
        if( 
            PDOexec($totalq)
            )
        {
            $Message = "Состояние аудитории $name обновлено";
//             echo "Success($id) : <BR>$totalq<BR>";
        }
        else
        {
            $Message = "Состояние аудитории $name не обновлено"; // */
//             echo "Error ($id) : <BR>$totalq<BR>";
        }
    
        // обновить дату изменения уч.плана
        PDOexec("CALL curriculum_altered($ctrl->curID);");

//     $Message = "Сохранено.";
}        

?>