<li><a href="index.php">Домой</a></li>
<li><a href="calc.php">Назад</a></li>
<head><title>Прогресс:ShedMe</title></head>

<pre>
<?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";

if(isset($_SESSION["shed"]))
{
    $shedID = $_SESSION["shed"];
}
else
{
    header('Refresh: 0; url=calc.php');
    exit("Choose Schedule first! Going back...");

}

    $shed_details = PDOfetch("SELECT status,message FROM `schedule` WHERE ID=$shedID;");
    $shed_status = $shed_details["status"];
    $shed_status_mapHr = array("" => "завершился неуспехом","ok" => "завершился успешно","calc" => "<i>в процессе</i>");

    $lpos_pos_array = PDOfetchAll("SELECT pos FROM `lessonpos` WHERE shedID=$shedID");

    $representation = "";
    $lessons_total = count($lpos_pos_array);
    $lessons_ok = 0;
    $lessons_queued = 0;
        
    foreach($lpos_pos_array as $el) {
        $pos = $el["pos"];
        if($pos >= 0)
        {
            $lessons_ok++;
            $representation .= "■";
        } elseif($pos <= -100) {
            $lessons_queued++;
            $representation .= "&#10023;"; // &#9635;
        } else {
            $representation .= "□";
        }
        
    }


//     print_r($shed_details);

    // LOG
    // SELECT text FROM debug ORDER BY ID;
    $log_size = PDOfetch("SELECT COUNT(ID) as cnt FROM `debug` WHERE 1")["cnt"];
    
    $last_N = 14;
    $first_row = max(0,$log_size - $last_N);
    $last_N = min($log_size, $last_N);
    
    $log_last_rows = PDOfetchAll("SELECT text FROM `debug` WHERE 1 LIMIT $first_row,$last_N");

?>
</pre>

<H2>Подождите, пока программа произведёт расчёт расписания...</H2>

<li>Состояние расчёта расписания: <b><?php echo $shed_status_mapHr[$shed_status] ?></b></li>
<h4>Сообщение расписания:</h4>
<pre><?php echo $shed_details["message"]?:"(нет сообщения)" ?></pre>

<?php if($lessons_total == 0) { ?>
    
    <h3>Подготовка...</h3>

<?php } else { ?>
    
    <h3>Прогресс: <?php echo round(100*$lessons_ok / $lessons_total,1) ?>%</h3>

    <pre><?php
    // BAR
    echo "[" . str_repeat("&#9658;", 60*$lessons_ok / $lessons_total) 
    . str_repeat("-", 60-60*$lessons_ok / $lessons_total) . "]";
    ?></pre>

Структура текущего состояния уроков:

<li>Всего: <b><?php echo $lessons_total ?></b></li>
<li>Расставлено: <b><?php echo $lessons_ok ?></b></li>
<li>В очереди: <b><?php echo $lessons_queued ?></b></li>
<li>Ожидает: <b><?php echo $lessons_total-$lessons_queued-$lessons_ok ?></b></li>

<p>
<pre><?php
    echo "<H2>[<b><big>" . $representation . "</big></b>]</H2>";
    ?></pre>

Обозначения:
<li><b>■</b> <i>Готов</i></li>
<li><b>&#10023;</b> <i>В очереди</i></li>
<li><b>□</b> <i>Ожидает</i></li>

<?php } ?>

<h4>Конец лога (<?php echo $last_N ?> строк из <?php echo $log_size ?>):</h4>
<pre><?php
    foreach($log_last_rows as $row)
        echo $row["text"]."\n";
?></pre>


<?php

if($shed_status == "calc")
{
    header('Refresh: 2; url=progress.php');
}
else
{
        $sesskey="Controller->message";
        $_SESSION[$sesskey] = "Процесс расчёта расписания " . $shed_status_mapHr[$shed_status];
    header('Refresh: 5; url=calc.php');
}
?>