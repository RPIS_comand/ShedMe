<?php

//// if( !isset($dbhandle) )
if( !isset($dbhandle) or empty($dbhandle) or is_null($dbhandle) )
{

    $host = 'localhost'; // адрес сервера   vds84.server-1.biz  localhost
    $database = 'shedme'; // имя базы данных
    $user = 'root'; // имя пользователя

    // подключаемся к серверу
    try {
        $dbhandle = new PDO("mysql:dbname=$database;host=$host", $user, 'our366Team' /*$password*/ );
        /// print_r("Connected!");
    } catch (PDOException $e) {
        die($e->getMessage());
    }
    
}

PDOexec("set names utf8");

///    print_r($dbhandle);

/***
Попробуй, поиграться вот с этими коммандами:
mysql_query("set character_set_connection=utf8;",$link);
mysql_query("set character_set_client=utf8;",$link);
mysql_query("set character_set_results=utf8",$link);
mysql_query("set names utf8",$link);

или чередуй эти комманды с другими кодировками: utf8 , cp1251

Вот краткое описание того, что они делают:
character_set_connection - указывает, в какую кодировку следует преобразовать данные полученые от клиента перед выполнением запроса 
character_set_client - указывает, в какой кодировке будут поступать данные от клиента 
character_set_results - указывает серверу не необходимость перекодировать результаты запроса в определенную кодировку перед выдачей их клиенту
set names - Если запрос и данные в таблице находятся в одинаковой кодировке и перекодировка резултата не требуется, то достаточно выполнить только это

***/


/// MySQL + PDO : `shortcuts` ///

function PDOexec($q)
{
    global $dbhandle;
//    $sth = $dbhandle->prepare($q);
//    return $sth->execute();
    return $dbhandle->exec($q); // number of affected rows is returned
}
function PDOfetch($q)
{
    global $dbhandle;
    $sth = $dbhandle->prepare($q);
    $sth->execute();
    return $sth->fetch(PDO::FETCH_ASSOC);
}
function PDOfetchAll($q)
{
    global $dbhandle;
    $sth = $dbhandle->prepare($q);
    $sth->execute();
    return $sth->fetchAll(PDO::FETCH_ASSOC);
}

?>
