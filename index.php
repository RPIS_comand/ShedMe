<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
      <title>Начало:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
        // подключаем общий фрагмент
        include_once "main_header.php"; // функции
    ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h1><b>Программа составления расписания</b><small><br>366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018 г</small></h1>

<pre>
<?php
     //// include_once "common/modals.html"; // подключаем общий фрагмент
      include_once "common.php"; // функции

    $curriculums = PDOfetchAll("SELECT * FROM `curriculum` ORDER BY date DESC");      
    
    if(isset($Message))
    {
        echo $Message;
    }

    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
?>
</pre>
      
      <h3>Выберите учебный план</h3>
      
      <table class="table table-condensed table-hover" style="width:70%">
      <thead>
        <tr>
          <th>№</th>
          <th>Учебный план</th>
          <th>Изменён</th>
          <th>Преподавателей</th>
          <th>Групп</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
          <!-- При клике по ссылке выбранный план становится текущим -->
        <?php foreach ($curriculums as $i => $cur) { ?>
        <tr <?php if(($ctrl->curID) and $ctrl->curID == $cur['ID']) echo 'class="success"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><a href="prof.php?cur=<?php echo $cur['ID'] ?>"><?php echo $cur['name'] ?></a></td>
          <td><?php echo $cur['date'] ?></td>
          <td><?php echo PDOfetch("SELECT COUNT(*) as cnt FROM `professor` WHERE curID=".$cur['ID'])['cnt']; ?></td>
          <td><?php echo PDOfetch("SELECT COUNT(*) as cnt FROM `group` WHERE curID=".$cur['ID'])['cnt']; ?></td>
            <td><button type="button" class="close" aria-hidden="true" onClick="var d=confirm('Удалить учебный план? \nИмя плана: <?php echo $cur['name'] ?>');if(d){window.location.href = '?del=<?php echo $cur['ID'] ?>';}"><img src='img/del.png' title="Удалить"></button></td>
            <!--<button type="button" class="close" aria-hidden="true">&times;</button> -->
        </tr>
        <?php } ?>
          
        <tr>
          <th><img src="img/add.png" /></th>
            <td><i><button type="button" class="btn btn-default" onClick="var n=prompt('имя:','Курс');/*alert(n)*/if(n){window.location.href = '?new='+n;}">Создать новый план</button></i></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tr>
          
      </tbody>
    </table>
      
      366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>