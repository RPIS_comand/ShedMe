<?php 

// юзер не представился
if(!$ctrl->usertype)
{
  header('Refresh: 0; url=whoareyou.php');
  exit("Please introduce yourself");
}


// добавить план
if(isset($_GET['new']))
{
    $name = stripslashes($_GET['new']);
    $name = trim($name);
    $name = htmlspecialchars($name, ENT_QUOTES);
    
    // добавить план + лимиты к нему
    // + добавить часы для выравнивания времени
    $ok = PDOexec("INSERT INTO `curriculum` (name) VALUES('$name');
    SELECT LAST_INSERT_ID() INTO @cur FROM curriculum LIMIT 1;
    UPDATE curriculum SET date=NOW()+INTERVAL 1 HOUR, altered=1 WHERE ID=@cur;
    INSERT INTO `limits` (curID) VALUES(@cur);");
    
    $ctrl->message = $ok ? "План добавлен" : "План НЕ был добавлен";
    
    header('Refresh: 0; url=index.php');
}
// удалить план
elseif(isset($_GET['del']))
{
    $id = stripslashes($_GET['del']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    if( PDOexec("DELETE FROM `curriculum` WHERE ID=$id;") )
        $ctrl->message = "Учебный план удалён!"; //// $_SESSION['Message']
    else
        $ctrl->message = "Ошибка удаления!";
    
    header('Refresh: 0; url=index.php');
}

/*
$curriculums = PDOfetchAll("SELECT * FROM `curriculum`");
print_r($curics);
    
$profs = PDOfetchAll("SELECT iD,name FROM `professor` ORDER BY `name`");
print_r($profs);
// */

?>