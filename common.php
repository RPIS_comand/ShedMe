<?php 

class Controller
{
    public $userID, $usertype, $usertitle, $groupID;
    public $message, $curID, $curname;
    
    function __construct()
    {
        // future // $this->userID = null;  // ID in table `user` or just 0
        $this->usertype = null; // `admin` `student` `professor` `room`
        $this->usertitle = null; // обращение к юзеру
        $this->profID = null;  // for professor only
        $this->groupID = null; // for student only
        $this->roomID = null; // for room only
        
        // unsetted 
        $this->message = null;
        $this->curID = null;
        $this->curname = null;
    }
    
    static function loadFromSESSION()
    {
        $c = new Controller;
        foreach($c as $key => $val)
        {
            // print_r("$key\n");
            $sesskey = "Controller->".$key;
            if(isset($_SESSION[$sesskey]))
                $c->{$key} = $_SESSION[$sesskey];
        }
        return $c;
    }
    function saveToSESSION()
    {
        foreach($this as $key => $val)
        {
            // print_r("$key\n");
            $sesskey = "Controller->".$key;
            $_SESSION[$sesskey] = $this->{$key};
        }
    }
    
    
}


/*
$curriculums = PDOfetchAll("SELECT * FROM `curriculum`");
print_r($curics);
    
$profs = PDOfetchAll("SELECT iD,name FROM `professor` ORDER BY `name`");
print_r($profs);
// */

/*
function getMessage()
{
    if(isset($_SESSION['Message']))
        return $_SESSION['Message'];
    else return false;
} // */


function insert_navigation_pills($which_page_active) {

?><!-- Навигация -->
<ul class="nav nav-pills">
  <li<?php if($which_page_active == "index")
    echo ' class="active"' ?>><a href="index.php">Домой</a></li>
  <li<?php if($which_page_active == "prof")
    echo ' class="active"' ?>><a href="prof.php">Преподаватели</a></li>
  <li<?php if($which_page_active == "group")
    echo ' class="active"' ?>><a href="group.php">Группы и занятия</a></li>
  <li<?php if($which_page_active == "summary")
    echo ' class="active"' ?>><a href="summary.php">Сводка</a></li>
  <li<?php if($which_page_active == "room")
    echo ' class="active"' ?>><a href="room.php">Ауд-рии</a></li>
  <li<?php if($which_page_active == "limit")
    echo ' class="active"' ?>><a href="limit.php">Ограничения</a></li>
  <li<?php if($which_page_active == "calc")
    echo ' class="active"' ?>><a href="calc.php">Расчёт</a></li>
  <li<?php if($which_page_active == "view")
    echo ' class="active"' ?>><a href="view.php">Просмотр</a></li>
</ul>      
<?php 

}

?>