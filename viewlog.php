<head><title>LOG</title></head>
<?php
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера

// кнопка "Очичстить лог"
// ...

echo "<table border=0>";

echo "<thead>
        <tr>
            <th>№:ID</th>
            <th>Время час назад</th>
            <th>-</th>
            <th>Сообщение</th>
        </tr>
    </thead>
<tbody>
";

$result = PDOfetchAll("SELECT ID,time,text FROM debug ORDER BY ID;");
foreach($result as $key => $row)
    echo "    
    <tr>
        <td>" .($key+1). ":" .$row["ID"]. "</td>
        <td>" .$row["time"]. "</td>
        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td><pre>" .$row["text"]. "</pre></td>
    </tr>";

echo "</tbody>
</table>";

?>