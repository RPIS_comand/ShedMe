<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
      <title>Кто Вы? ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        /* if(isset($_SESSION['Message']))
        {
            $Message = $_SESSION['Message'];
            unset($_SESSION['Message']);
        } // */
    ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3>Программа составления расписания</h3>

<pre>
<?php
     //// include_once "common/modals.html"; // подключаем общий фрагмент
     include_once "whoareyou_header.php"; // функции

    //$curriculums = PDOfetchAll("SELECT * FROM `curriculum` ORDER BY date DESC");      
    
    if(!is_null($ctrl->message))
    {
        echo $ctrl->message;
        $ctrl->message = null;
    } // */

    // $curriculums = PDOfetchAll("SELECT * FROM `curriculum` ORDER BY date DESC");      

    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
?>
</pre>
      
      <h3><b>Представьтесь Программе</b></h3>
      
<div class="table-responsive">
    <table class="table table-bordered table-striped">
      <colgroup>
        <col class="col-xs-1">
        <col class="col-xs-7">
      </colgroup>
      <thead>
        <tr>
          <th>Кто Вы?</th>
          <th>Выберите себя</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Вы управляете учебными планами</td>
            <td><a href="?i=admin">Администратор</a></td>
        </tr>
        <tr>
          <td>Вы - преподаватель</td>
            <td><?php 
              $curs = PDOfetchAll("SELECT ID,name FROM `curriculum` ORDER BY name");
          foreach($curs as $cur_elem) {
              ?><ul class="list-inline"><i><?php echo $cur_elem['name']?></i><p><?php 
              $recs = PDOfetchAll("SELECT ID,name,curID FROM `professor` WHERE curID=".$cur_elem['ID']." ORDER BY name");
              // print_r($recs);
              foreach($recs as $prep_elem) {
                  echo "<li><a href='?i=professor&cur=".$prep_elem['curID']."&id=".$prep_elem['ID']."'>".$prep_elem['name']."</a></li>";
              } ?>
              </ul>
      <?php }
              ?></td>
        </tr>
        <tr>
          <td>Вы - студент</td>
            <td>Найдите свою группу (<small>формат: </small><code>[курс] группа</code>):<p><?php 
              //// $curs = PDOfetchAll("SELECT ID,name FROM `curriculum` ORDER BY name");
          foreach($curs as $cur_elem) {
              ?><ul class="list-inline"><i><?php echo $cur_elem['name']?></i><p><?php 
              $recs = PDOfetchAll("SELECT ID,name,curID,course FROM `group` WHERE curID=".$cur_elem['ID']." ORDER BY course,name");
              // print_r($recs);
              foreach($recs as $group_elem) {
                  echo "<li><a href='?i=student&cur=".$group_elem['curID']."&group=".$group_elem['ID']."'>".
                      (0==($group_elem['course']) ? "" : "[".$group_elem['course']."] ")
                      .$group_elem['name'].
                      "</a></li>";
              } ?>
              </ul>
      <?php }
              ?></td>
        </tr>
        <tr>
          <td>Вы желаете посетить аудиторию</td>
            <td>Найдите нужную аудиторию:<p><?php 
              //// $curs = PDOfetchAll("SELECT ID,name FROM `curriculum` ORDER BY name");
          foreach($curs as $cur_elem) {
              ?><ul class="list-inline"><i><?php echo $cur_elem['name']?></i><p><?php 
              $recs = PDOfetchAll("SELECT ID,curID,name FROM `room` WHERE curID=".$cur_elem['ID']." ORDER BY ID");
              // print_r($recs);
              foreach($recs as $room_elem) {
                  echo "<li><a href='?i=room&cur=".$room_elem['curID']."&room=".$room_elem['ID']."'>"
                      .$room_elem['name'].
                      "</a></li>";
              } ?>
              </ul>
      <?php }
              ?></td>
        </tr>
      </tbody>
    </table>
  </div>      
      
      <p><small>366-РПИС-2 Программа составления расписания</small></p>
      
  </div>
  </body>
</html>