<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>Группы:ShedMe</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php
    // Включение вывода всех ошибок и предупреждений в коде PHP-скриптов
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    //setlocale(LC_ALL, 'ru_RU');

        //  вся процедура работает на сессиях.
        session_start();
        require_once "connection.php";
        include_once "common.php"; // функции юзера
        //* get Controller
        $ctrl = Controller::loadFromSESSION();

        // get last Message
        if($ctrl->message)
        {
            $Message = $ctrl->message;
            $ctrl->message = null;
        }
      
         // подключаем общий фрагмент
         include_once "group_header.php";

        // SAVE
        $ctrl->saveToSESSION();
        // SAVE
  ?>
  </head>
  <body>
<?php
      include_once "pagehead.php"; // Заголовок
?>

  <div class="container-fluid">
        
      <!-- 366-2 (Денисов, Головинова, Михайлов, Ибрагим) 2018г -->
          <h3><b>Программа составления расписания</b> <small><a href="index.php">Домой</a></small></h3>
          <h5>Текущий учебный план: <b><?php echo $ctrl->curname ?></b> <small><a href="index.php">Другой...</a></small></h5>

<!-- Навигация -->
<?php
    insert_navigation_pills("group");
?>
      
<pre><?php
     //// include_once "common/modals.html"; // подключаем общий фрагмент
      include_once "common.php"; // функции

    $records = PDOfetchAll("SELECT * FROM `group` WHERE curID=$ctrl->curID");
              //////  ORDER BY name ASC
    /// echo('---<br>');
    //print_r("POST: ");
    //print_r($_POST);
              
      // ! if empty ?!
   
///    SELECT * FROM lessonplan as lp,`subject` as s,professor as p WHERE lp.groupID=4 AND s.ID=lp.subjID AND p.ID=s.profID -- ORDER BY course,name
              
    if(count($records) > 0 && $groupID) {
        $group = $records[array_search($groupID, array_column($records, 'ID'))]; 
        $groupname = $group['name']; 
        $nestname = false;

        // "вложенные" записи - предметы и планы занятий, ассоциированные с группами
        $nestedrecs = PDOfetchAll("SELECT subjID,profID,lp.ID as lpID,course,lec,prac,lab,s.name as subjname,p.name as profname FROM lessonplan as lp,`subject` as s,professor as p WHERE lp.groupID=$groupID AND s.ID=lp.subjID AND p.ID=s.profID ORDER BY subjname,profname");
        /// print_r($nestedrecs);
        if(count($nestedrecs) > 0 && $subjID) {
            $nestrec = $nestedrecs[array_search($subjID, array_column($nestedrecs, 'subjID'))];
            $nestname = $nestrec['subjname']; 
            $nestcourse = $nestrec['course']; 
            // NULL -> 0
            $nestcourse = is_null($nestcourse)? 0 : $nestcourse;
            /// print_r($nestrec);
            //// unset($nestrec);
        }
    }
              
    if(isset($Message))
    {
        echo $Message;
    }
   //echo "\$groupID=".$groupID;
    //// $key = array_search(40489, array_column($userdb, 'uid'));
    ///  print_r(array_column($records, 'name'));
              
    ///  print_r(array_search($groupID, array_column($records, 'ID')));
    // print_r ($nestedrecs);
?></pre>
      
<h3> 
      
<ol class="breadcrumb">
  <li><a href="group.php">Группы</a></li>
  <?php if($groupID) { ?>
    <li <?php if( ! $subjID) echo 'class="active"' ?>>
        <a href="group.php?g=<?php echo $groupID ?>"
        ><?php echo $groupname ?></a></li>
    <?php } ?>
  <?php if($subjID) { ?>
    <li <?php echo 'class="active"' ?>>
        <a href="group.php?g=<?php echo $groupID ?>&s=<?php echo $subjID ?>"
        ><?php echo $nestname ?></a></li>
    <?php } ?>
  <?php if($modeCOMB) { ?>
    <li <?php echo 'class="active"' ?>>
        <a href="group.php?g=<?php echo $groupID ?>&s=<?php echo $subjID ?>&comb=1"
        >Объединение лекций</a></li>
    <?php } ?>
</ol>      
      
</h3>
      
      <!-- Расположить таблицы рядом по горизонтали text-center -->
<div class="container-fluid content">
  <div class="row">
      
    <?php if( ! $modeCOMB) { // не режим комбинирования лекций ?>
  <div class="col-md-3  col-sm-3">
      <table class="table table-condensed table-hover" style="width:20%">
      <thead>
        <tr>
          <th>№</th>
          <th>Группа</th>
          <td><small><i>Удалить</i></small></td>
        </tr>
      </thead>
      <tbody>
          <!-- При клике по ссылке выбранная Группа становится текущей -->
        <?php foreach ($records as $i => $rec) { ?>
        <tr <?php if(($groupID) and $groupID == $rec['ID']) echo 'class="success"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><a href="?g=<?php echo $rec['ID'] ?>"><?php echo $rec['name'] ?></a></td>
          <td><button type="button" class="close" aria-hidden="true" onClick="var d=confirm('Удалить группу? \nИмя группы: <?php echo $rec['name'] ?>');if(d){window.location.href = '?del=<?php echo $rec['ID'] ?>';}"><img src='img/del.png' title="Удалить" alt="Удалить"></button></td>
        </tr>
        <?php } ?>
          
        <tr>
          <th><img src="img/add.png" /></th>
            <td colspan="2"><i><button type="button" class="btn btn-default" onClick="var n=prompt('Краткое наименование группы (до 20 символов):','ШИФР-код');if(n){window.location.href = '?new='+n.slice(0,29);}">Создать группу</button></i></td>
          <!-- <td></td> -->
        </tr>
          
      </tbody>
    </table>
 </div>
<?php } ?>
      
    <?php if($groupID) { // группа выбрана ?>
<div class="col-md-4 col-sm-4">
    <u><h4><small>Группа</small> <?php echo $groupname ?></h4></u>
        № курса группы (<?php echo ($group['course']==0)? 'не выбран' : $group['course']; ?>):<br>
        <form>
        <select name='course' class="form-control" onChange="/*alert(course.selectedIndex);*/if(course.selectedIndex!=<?php echo $group['course'] ?>){window.location.href = 'group.php?g=<?php echo $groupID ?>&course='+course.selectedIndex;};">
          <option value='0' <?php if($group['course']==0) echo 'selected'; ?> >Не выбрано</option>
          <option value='1' <?php if($group['course']==1) echo 'selected'; ?> >1</option>
          <option value='2' <?php if($group['course']==2) echo 'selected'; ?> >2</option>
          <option value='3' <?php if($group['course']==3) echo 'selected'; ?> >3</option>
          <option value='4' <?php if($group['course']==4) echo 'selected'; ?> >4</option>
          <option value='5' <?php if($group['course']==5) echo 'selected'; ?> >5</option>
          <option value='6' <?php if($group['course']==6) echo 'selected'; ?> >6</option>
        </select>
        </form>
    
        <br>
      <table class="table table-condensed table-hover" style="width:20%">
      <thead>
        <tr>
          <th>№</th>
          <th>Предмет</th>
          <th>Курс</th>
          <th colspan=2>Преподаватель</th>
          <!-- <td ><small><i>Удалить</i></small></td> -->
        </tr>
      </thead>
      <tbody>
          <!-- При клике по ссылке выбранный Предмет становится текущим -->
        <?php foreach ($nestedrecs as $i => $rec) { ?>
        <tr <?php if(($subjID) and $subjID == $rec['subjID']) echo 'class="success"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><a href="?g=<?php echo $groupID ?>&s=<?php echo $rec['subjID'] ?>"><?php  echo $rec['subjname']; ?></a></td>
          <td><?php echo ($rec['course']==0)? "-" : $rec['course'];  ?></td>
          <td><?php echo $rec['profname']; ?> <a href="prof.php?p=<?php echo $rec['profID'] ?>&s=<?php echo $rec['subjID'] ?>" target="_blank" title="Редактировать преподавателя" alt="Преподаватель"><big>&#9998;</big></a>
              <!-- <a href="prof.php?p=<?php echo $rec['profID'] ?>&s=<?php echo $rec['subjID'] ?>" target="_blank"><button><big>&#9998;</big></button></a> --></td>
          <td><button type="button" class="close" aria-hidden="true" onClick="var d=confirm('Убрать предмет из расписания для группы <?php echo $groupname ?>? \nНазвание предмета: <?php echo $rec['subjname'] ?> \nИмя преподавателя: <?php echo $rec['profname'] ?>');if(d){window.location.href = 'group.php?g=<?php echo $groupID ?>&s=<?php echo $subjID ?>&delnest=<?php echo $rec['subjID'] ?>&lp=<?php echo $rec['lpID'] ?>';}"><img src='img/del.png' title="Удалить" alt="Удалить"></button></td>
        </tr>
        <?php } ?>
          
        <tr>
          <th><img src="img/add.png" /></th>
          <td colspan="3"><i><button type="button" class="btn btn-default" onClick="window.location.href = 'group.php?g=<?php echo $groupID ?>&add=1'">Добавить предмет &#9658;</button></i></td>
          <td></td>
        </tr>
          
      </tbody>
    </table>
 </div>
    <?php } ?>

    <?php if( ! $modeCOMB && !$modeADD && $subjID) { // предмет выбран -  редакция кол-ва лекций, практик и лаб
      
      ?>
    <div class="col-md-4 col-sm-4">    
    <u><h4><small>Дисциплина</small> <?php echo $nestname ?></h4></u>
        Курс: <b><?php echo ($nestcourse==0)? "-" : $nestcourse;  ?></b><br>
        План занятий:
        <form method="POST" action="">
            <div class="form-group text-center">

        <input type="hidden" name="type" value="plan">
        <input type="hidden" name="lpID" value="<?php echo $nestrec['lpID'] ?>">
                
        <p class="text-left indent">Лекций:</p>
        <input type="number" name="lec" class="form-control indent" min="0" value="<?php echo $nestrec['lec'] ?>" required>
        <p class="text-left indent">Практик:</p>
        <input type="number" name="prac" class="form-control indent" min="0" value="<?php echo $nestrec['prac'] ?>" required>
        <p class="text-left indent">Лабораторных работ:</p>
        <input type="number" name="lab" class="form-control indent" min="0" value="<?php echo $nestrec['lab'] ?>" required>
            </div>
            <div class="logModal-footer">
                <input type="submit" name="submit" class="btn btn-primary" value="Сохранить">
            </div>
        </form>
        
        <?php if( ! $modeCOMB) { // НЕ режим комбинирования лекций ?>
        <a href="group.php?g=<?php echo $groupID ?>&s=<?php echo $subjID ?>&comb=1"><button><big>&#9998; Объединить лекции...</big></button></a><br>
        <?php }
            //*
            // комбинации групп, уже объединённых по текущему предмету
            // , их количество
            // $combs = PDOfetchAll("SELECT COUNT(ID) FROM `combination` WHERE subjID=$subjID");
        $cnt = PDOfetch("SELECT COUNT(combID) as cnt FROM `combinedlecture`,`combination` WHERE `combinedlecture`.combID=`combination`.ID AND `combination`.subjID=$subjID AND `combinedlecture`.groupID=$groupID")["cnt"];
            // unset($recs);
            /// print_r($cnt);
            if($cnt>0) { echo "(Группа состоит в $cnt объед.)"; }
            else { echo "(Группа не состоит в одном объединении)"; }
                //*/
            unset($cnt);
         ?>
        
    </div>
    <?php } ?>
      
      
    <?php if($modeCOMB) { // режим комбинирования лекций ?>
    <div class="col-md-4 col-sm-4">
        <h4><a href="group.php?g=<?php echo $groupID ?>&s=<?php echo $subjID ?>"><button><big>Назад</big></button></a></h4>
        
        <u><h4>Создать новое объединение</h4></u>
        
    <?php
        // группы, "подписанные" к текущему предмету
        $recs = PDOfetchAll("SELECT groupID,`group`.name,`group`.course FROM `lessonplan`,`group` WHERE subjID=$subjID AND `lessonplan`.groupID=`group`.ID ORDER BY `group`.course,`group`.name");
        // unset($recs);
        if(count($recs)>=2) { 
    ?>
                    <form action="" method="post" class="form-horizontal" role="form">
                        <div class="form-group text-left">
                        <!-- <h3 class="indent">New order</h3> -->
                            <input type="hidden" name="type" value="newcomb">
                            <!-- <input type="hidden" name="subjID" value="<?php echo $subjID ?>"> -->
                            <!-- <p class="text-left indent">Количество общих Лекций:</p>
                            <input type="number" name="lec" class="form-control indent" min="0" value="1" required> -->
                      <div class="form-group">
                        <label class="col-sm-6 control-label">Общих лекций:</label>
                        <div class="col-sm-6">
                          <input type="number" class="form-control" name="lec" placeholder="Лекций" min="0" value="2" required>
                        </div>
                      </div>
                            <!-- <p class="text-left indent">Объединяемые группы:</p> -->
                  <div class="form-group">
                    <label class="col-sm-6 control-label">Объединяемые группы:</label>
                    <div class="col-sm-6">
                            <fieldset id="UserType">
                                
                  <?php foreach($recs as $rec) { ?>
                       <label class="font-unset">
                           <input type="checkbox" name="combgrID[]" value="<?php echo $rec['groupID']?>" <?php if($rec['groupID']==$groupID)echo "checked" ?>>
                                <?php //echo $rec['name']
                              echo "<a href='group.php?g=".$rec['groupID']."&s=$subjID'>".
                                  (!($rec['course']) ? "" : "[".$rec['course']."] ")
                                  .$rec['name'].
                                  " &#9998;</a>"; ?>
                       </label>
                  <?php } ?>
                                
                            </fieldset>
                        </div>
                      </div>
                            
                        </div>
                        <div class="logModal-footer">
                            <input type="submit" class="btn btn-primary" value="&nbsp; &nbsp; &nbsp; Создать &nbsp; &nbsp; &nbsp;">
                        </div>
                    </form>
            <?php } 
                 else {
                     echo "(Нет групп для объединения)";
                  }?>
        
        <u><h4>Существующие объединения</h4></u>
        
        <?php
            // комбинации групп, уже объединённых по текущему предмету
            $combs = PDOfetchAll("SELECT ID,lec FROM `combination` WHERE subjID=$subjID");
            // unset($recs);
            if(count($combs)>0) { 
        ?>
              <?php foreach($combs as $comb) {
                $onNlec = "на ".$comb['lec'].($comb['lec']<=4?" лекции":" лекций");
                  ?><ul class="list-inline"><?php echo "<h5>&bullet; <i>Объединение</i> <small>$onNlec</small> " ?>
            <!-- <form method="POST" action="">
                <input type="hidden" name="type" value="delcomb">
                <input type="hidden" name="combID" value="<?php echo $comb['ID'] ?>">                 
                <input type="submit" class="btn btn-normal" value="Удалить">
            </form> -->
        <!-- Вариант 1 -->
        <button onClick="if(confirm('Удалить объединение групп <?php echo $onNlec ?>?')){location.href='<?php echo "group.php?g=$groupID&s=$subjID&delcomb=".$comb['ID']; ?>'}" title="Удалить объединение" alt="Удалить">Удалить</button>
        <!-- Вариант 2 -->
        <!-- <button type="button" class="close" aria-hidden="true" onClick="if(confirm('Удалить объединение групп <?php echo $onNlec ?>?')){location.href='<?php echo "group.php?g=$groupID&s=$subjID&delcomb=".$comb['ID']; ?>'}"><img src='img/del.png' title="Удалить" alt="Удалить"></button>  -->
        <?php echo'</h5>';
                  
                $recs = PDOfetchAll("SELECT `combinedlecture`.ID,groupID,`group`.name,`group`.course FROM `combinedlecture`,`group` WHERE combID=".$comb['ID']." AND `combinedlecture`.groupID=`group`.ID ORDER BY `group`.course,`group`.name");
                  
                    foreach($recs as $rec) {
                      if($groupID == $rec['groupID']) {
                          echo "<li><u>".
                          (!($rec['course']) ? "" : "[".$rec['course']."] ")
                          .$rec['name'];
                          ?> <button onClick="if(confirm('Удалить текущую группу <?php echo $rec['name'] ?> из объединения групп <?php echo $onNlec ?>?')){location.href='<?php echo "group.php?g=$groupID&s=$subjID&delfromcomb=".$comb['ID']."&which=".$rec['ID']; ?>'}" title="Удалить группу" alt="Удалить">&#10008;</button><?php echo "</u></li>&nbsp;&nbsp;";
                      } else {
                          echo "<li><a href='group.php?g=".$rec['groupID']."&s=$subjID&comb=1'>".
                          (!($rec['course']) ? "" : "[".$rec['course']."] ")
                          .$rec['name']."</a></li>";
                      }
                    } // */
                  ?></ul><?php
                 } ?>
        <?php } 
             else {
                 echo "(Не создано ни одного объединения для этого предмета)";
              }  ?>
    </div>
    <?php }?>
      
    <?php if($modeADD) { // ДОБАВКА ПРЕДМЕТОВ 
      $suggests = null;
      $course = $group['course'];
        // Кандидаты на добавку группе - предметы того же курса + и без курса
        $suggests = PDOfetchAll("SELECT s.ID as subjID,profID,course,s.name as subjname,p.name as profname FROM `subject` as s,professor as p WHERE p.curID=$ctrl->curID AND p.ID=s.profID AND ($course=0 OR s.course=$course OR s.course=0) AND NOT EXISTS (SELECT ID FROM lessonplan WHERE groupID=$groupID AND lessonplan.subjID=s.ID)  ORDER BY course DESC,subjname ASC");
        /// print_r($nestedrecs);
      ?>
      
<div class="col-md-4 col-sm-4">
    <?php echo'<h4>' ?>Доступные к добавлению предметы<?php 
    if(count($suggests)==0) {
          echo " отсутствуют.</h4>"; } 
    else { echo ":</h4>"; ?>
    
        <br>
      <table class="table table-condensed table-hover" style="width:20%">
      <thead>
        <tr>
          <th>№</th>
          <th>Предмет</th>
          <th>Курс</th>
            <th colspan=2>Преподаватель</th>
        </tr>
      </thead>
      <tbody>
          <!-- При клике по ссылке выбранный Предмет становится текущим -->
        <?php foreach ($suggests as $i => $rec) { ?>
        <tr <?php if(($subjID) and $subjID == $rec['subjID']) echo 'class="warning"' ?>>
          <td><?php echo $i+1 ?></td>
          <td><!--<a href="?g=<?php echo $groupID ?>&s=<?php echo $rec['subjID'] ?>">--><?php  echo $rec['subjname']; ?><!--</a>--></td>
          <td><?php echo ($rec['course']==0)? "-" : $rec['course'];  ?></td>
          <td><?php echo $rec['profname']; ?> <a href="prof.php?p=<?php echo $rec['profID'] ?>&s=<?php echo $rec['subjID'] ?>" target="_blank" title="Редактировать предметы предавателя" alt="Преп."><big>&#9998;</big></a>
              <!-- <a href="prof.php?p=<?php echo $rec['profID'] ?>&s=<?php echo $rec['subjID'] ?>" target="_blank"><button><big>&#9998;</big></button></a> --></td>
          <td><button type="button"  aria-hidden="true" onClick="window.location.href = 'group.php?g=<?php echo $groupID ?>&newnest=<?php echo $rec['subjID'] ?>';">Добав.<big><img src='img/add.png' title="Добавить" alt="Добавить"></big></button></td>
        </tr>
        <?php }} ?>
          
      </tbody>
    </table>
 </div>
    <?php } ?>

 </div>
</div>
 
  366-РПИС-2
<br>Программа составления расписания
      
  </div>
  </body>
</html>