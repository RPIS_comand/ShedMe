<?php 

// Текущие - сброшены по умолчанию
$groupID = false;
$subjID = false;
$modeADD = false;
$modeCOMB = false;

// Set current curriculum 
if(isset($_GET['cur']))
{
    $id = stripslashes($_GET['cur']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    $ctrl->curID = $id;
    $ctrl->curname = PDOfetch("SELECT name FROM `curriculum` WHERE ID=$id")['name'];
    
    header('Refresh: 0; url=group.php');
    
    $ctrl->saveToSESSION();
    exit("Loading Curriculum...<br>Wait...");
}
elseif( ! ($ctrl->curID) )
{
    header('Refresh: 4; url=index.php'); // GO Home
    exit("Choose Curriculum first! Going Home...");
    
}

// выбрать 
if(isset($_GET['g']))
{
    $id = stripslashes($_GET['g']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
  // !!!  $ctrl->profID = $id;
    $groupID = $id;
}
// выбрать 
if(isset($_GET['s']))
{
    $id = stripslashes($_GET['s']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
  // !!!  $ctrl->profID = $id;
    $subjID = $id;
}
// режим
if(isset($_GET['add']) and $groupID)
{
    $modeADD = true;
}
// режим
if(isset($_GET['comb']) && $groupID && $subjID)
{
    $modeCOMB = true;
    $modeADD = false;
}


// добавить (группу)
if(isset($_GET['new']))
{
    $name = stripslashes($_GET['new']);
    $name = trim($name);
    $name = htmlspecialchars($name, ENT_QUOTES);
    
    PDOexec("INSERT INTO `group` (curID,name) VALUES($ctrl->curID,'$name');");
    $newid = PDOfetch("SELECT LAST_INSERT_ID() as newid FROM `group`;")['newid'];
    
    header("Refresh: 0; url=group.php?g=$newid");
    exit("Adding new group...");
}
// удалить  (группу)
elseif(isset($_GET['del']))
{
    $id = stripslashes($_GET['del']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `group` WHERE curID=$ctrl->curID AND ID=$id;") )
        $ctrl->message = "Группа удалена!";
    else
        $ctrl->message = "Ошибка удаления!"; // */
    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
    
    header('Refresh: 0; url=group.php');
    exit("Deleting a group...");
}
// обновить 
if($groupID && isset($_GET['course']))
{
    $n = stripslashes($_GET['course']);
    $n = trim($n);
    $n = htmlspecialchars($n, ENT_QUOTES);
    
    PDOexec("UPDATE `group` SET course=$n WHERE ID=$groupID;");
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    
    header("Refresh: 0; url=group.php?g=$groupID");
    exit("Updating a group`s course: $n...");
}


// добавить ВЛОЖЕННЫЙ ЭЛЕМЕНТ (назначить предмет)
if($groupID && isset($_GET['newnest']))
{
    // subject.ID
    $s_id = stripslashes($_GET['newnest']);
    $s_id = trim($s_id);
    $s_id = htmlspecialchars($s_id, ENT_QUOTES);
    
    PDOexec("INSERT INTO `lessonplan` (subjID,groupID) VALUES($s_id,'$groupID');");
    
    //// $newid = PDOfetch("SELECT LAST_INSERT_ID() as newid FROM `lessonplan`;")['newid'];
    
    // ! 
    PDOexec("CALL curriculum_altered($ctrl->curID);");

    $ctrl->message = "Предмет назначен.";
    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
    
    header("Refresh: 0; url=group.php?g=$groupID&s=$s_id");
    unset($newid);
    exit("Assgning a subject...");
}
// удалить ВЛОЖЕННЫЙ ЭЛЕМЕНТ
elseif( isset($_GET['delnest']) && isset($_GET['lp'])  && isset($subjID) )
{
    // subject.ID
    $s_id = stripslashes($_GET['delnest']);
    $s_id = trim($s_id);
    $s_id = htmlspecialchars($s_id, ENT_QUOTES);
    // lessonplan.ID
    $id = stripslashes($_GET['lp']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `lessonplan` WHERE groupID=$groupID AND ID=$id;") && PDOexec("DELETE FROM `combinedlecture` WHERE groupID=$groupID AND EXISTS(SELECT ID FROM `combination` WHERE  combinedlecture.combID = combination.ID AND combination.subjID=$subjID);") )
    {
        $ctrl->message = "Предмет убран!";
        // обновить дату
        PDOexec("CALL curriculum_altered($ctrl->curID);");
    }
    else
        $ctrl->message = "Ошибка удаления! ".$subjID; // */
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
    
    header("Refresh: 0; url=group.php?g=$groupID&s=$s_id&add=1");
    exit("Unsetting a subject for group...");
}
// обновить кол-во лекций, практик и лаб
if($groupID && $subjID && isset($_POST['type']) && $_POST['type']=="plan" && isset($_POST['lpID']))
{
    PDOexec("UPDATE `lessonplan` SET lec=".$_POST['lec'].", prac=".$_POST['prac'].", lab=".$_POST['lab']." WHERE ID=".$_POST['lpID'].";");
    
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    
    header("Refresh: 0; url=group.php?g=$groupID&s=$subjID"); // 
    exit("Updating a group`s plan...");
}

// новое объединение
if($groupID && $subjID && isset($_POST['type']) && $_POST['type']=="newcomb")
{
    if(! isset($_POST['combgrID']) or count($_POST['combgrID'])<2) {
        // $ctrl->message = "Ошибка: выберите 2 или более групп, чтобы создать объединение групп на одном занятии!"; // 
        // SAVE
        $ctrl->saveToSESSION();
        // SAVE */
        $Message = "Ошибка: выберите 2 или более групп, чтобы создать объединение групп на одном занятии!";

        //header("Refresh: 0; url=group.php?g=$groupID&s=$subjID&comb=1");
        //exit("Try make combination for groups again.");
    }
    
    $lec = $_POST['lec'];
    
    PDOexec("INSERT INTO `combination` (subjID,lec) VALUES($subjID,$lec);");
    
    $newid = PDOfetch("SELECT LAST_INSERT_ID() as newid FROM `combination`;")['newid'];
    /// echo "\$newid:$newid";
    
    $ins_data = "";
    foreach($_POST['combgrID'] as $grID) {
        $ins_data .= (empty($ins_data)? "" : "),(") . $newid .",". $grID;
    }
    /// echo "\$ins_data:$ins_data ";
    PDOexec("INSERT INTO `combinedlecture` (combID,groupID) VALUES($ins_data);");

    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");
    
    // header("Refresh: 0; url=group.php?g=$groupID&s=$subjID&comb=1"); // 
    // exit("Adding new groups` combination...");
}
// удалить объединение
elseif($groupID && $subjID && isset($_GET['delcomb']))
{
    $id = stripslashes($_GET['delcomb']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `combination` WHERE ID=$id;") )
        $ctrl->message = "Объединение удалено!";
    else
        $ctrl->message = "Ошибка удаления!"; // */
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");

    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
    
    header("Refresh: 0; url=group.php?g=$groupID&s=$subjID&comb=1");
    exit("Deleting a combination...");
}
// удалить ИЗ объединения
elseif($groupID && $subjID && isset($_GET['delfromcomb']) && isset($_GET['which']))
{
    // combination.ID
    $id = stripslashes($_GET['delfromcomb']);
    $id = trim($id);
    $id = htmlspecialchars($id, ENT_QUOTES);
    // combinedlecture.ID
    $w_id = trim(stripslashes($_GET['which']));
    $w_id = htmlspecialchars($w_id, ENT_QUOTES);
    
    //*
    if( PDOexec("DELETE FROM `combinedlecture` WHERE combID=$id AND ID=$w_id;") )
        $ctrl->message = "Группа из объединения удалена!";
    else
        $ctrl->message = "Ошибка удаления!"; // */
    // обновить дату изменения уч.плана
    PDOexec("CALL curriculum_altered($ctrl->curID);");

    // SAVE
    $ctrl->saveToSESSION();
    // SAVE
    
    header("Refresh: 0; url=group.php?g=$groupID&s=$subjID&comb=1");
    exit("Deleting group from combination...");
}

?>